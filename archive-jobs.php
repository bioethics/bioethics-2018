<?php 
require_once('header.php'); 

$jobs = get_current_jobs();

$previous = get_adjacent_post( false, '', true,'' );
$pages_total = intval($jobs->max_num_pages);
$paged_var = intval(get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1);

?>
<div class="row jobs-container">

	<!-- Modal -->
<div id="jobModal" class="modal fade detail-view-modal" role="dialog">
    <div class="modal-dialog">

	        <!-- Modal content-->
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title"></h4>
	            </div>
	            <div class="modal-body">
	                <b class="job-posted"></b>
	                <table class="table">
	                    <tbody>
	                        <tr>
	                            <td>DEADLINE:</td>
	                            <td class="job-deadline"></td>
	                        </tr>
	                        <tr>
	                            <td>LOCATION:</td>
	                            <td class="job-location"></td>
	                        </tr>

	                        <tr>
	                            <td>
	                                CONTACT:
	                            </td>
	                            <td class="job-contact"></td>
	                        </tr>
	                        <tr>
	                            <td>
	                                EMAIL:
	                            </td>
	                            <td class="job-email">
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                TELEPHONE:
	                            </td>
	                            <td class="job-tel">
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <div><b>DETAILS:</b></div>
	                <div class="job-details post-details">
	                </div>
	                <a class="job-permalink" href="#">Permanent Link</a>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            </div>
	        </div>

	    </div>
	</div><!-- End of jobModal -->

	<div class="left-div col-lg-12 col-md-12 col-sm-12">
		<?php 
		if ( $jobs) : ?>
			<h3 class="section-title title">
	            <span class="hidden-xs">Bioethics Jobs.</span>
	            <span class="hidden-lg hidden-md hidden-sm">Jobs.</span>
	            <div class="btn btn-white" data-toggle="collapse" data-target=".submit-job">
	            	<a href="<?php bloginfo('wpurl')?>/submit_job" id="subnav-submit">Submit a Job</a>
	            </div>
	            <div class="clearfix"></div>
	            <div class="bg-title"></div>
	        </h3>

	        <div class="row">
			    <form class="form-horizontal" name="sortJobs" method="post">
			        <div class="form-group col-xs-12 col-sm-4">
			            <label for="sortBy" class="control-label col-md-4 col-sm-4">Sort By</label>
			            <div class=" col-md-8 col-sm-8">
			                <select name="sortBy" id="sortBy" class="form-control" style="width:100%;">
			                    <option <?php echo ($_POST['sortBy'] == 'city') ? 'selected' : '' ?>  value="city">Location</option>
			                    <option <?php echo ($_POST['sortBy'] != 'city')  ? 'selected' : '' ?>  value="deadline">Deadline</option>
			                </select>
			            </div>
			        </div>
			        <div class="form-group col-xs-12 col-sm-4">
			            <label for="searchby" class="control-label col-md-4 col-sm-4">Search Jobs</label>
			            <div class=" col-md-8 col-sm-8">
			                <input type="text" class="form-control" id="searchBy" name="searchBy" value="<?= $_POST['searchBy']?>">
			            </div>
			        </div>		        
			       	<div class="form-group col-xs-12 col-sm-4">
			       		<div class="col-md-4 col-sm-12">
			            	<input type="submit" name="Search" value="Search" class="form-control btn btn-primary">
			            </div>
			        </div>
			    </form>
			</div>
			
			<?php require_once('loop-jobs.php'); ?>

			<div id="pagination" class="clear-both col-sm-12">
				<span class="nav-old">
				<?php
					if($pages_total > 0 && $paged_var !== $pages_total){
						next_posts_link( '<span class="meta-nav">&larr;</span> Next' );
					}
				?>
				</span>
				<span class="nav-new"><?php previous_posts_link( 'Previous <span class="meta-nav">&rarr;</span>' ); ?></span>
			</div>
		<?php else : ?>
			<p>No posts found.</p>
		<?php endif; 
	?>
	</div>
</div><!--end content-->
<?php require_once('footer.php'); ?>

<script src="<?php echo get_template_directory_uri(); ?>/vendor/select2/js/select2.full.min.js" type="application/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/vendor/select2/css/select2.min.css">
<script type="text/javascript">
	$(document).ready(function() {
        $('#sortBy').change(function() {
	        $('form[name=sortJobs]').submit();
	    });
    });    

    function showjobsModal(modalData)
	{
	   //you can do anything with data, or pass more data to this function. i set this data to modal header for example
	   $("#jobModal .modal-title").html(modalData.title);
	   $("#jobModal .job-company").html(modalData.company);
	   $("#jobModal .job-deadline").html(modalData.deadline);
	   $("#jobModal .job-location").html(modalData.location);
	   $("#jobModal .job-details").html(modalData.description);
	   $("#jobModal .job-contact").html(modalData.name);
	   $("#jobModal .job-posted").html(modalData.posted);
	   $("#jobModal .job-email").html('<a href="mailto:'+modalData.email+'">'+modalData.email+'</a>');
	   $("#jobModal .job-tel").html('<a hreftel=":'+modalData.tel+'">'+modalData.tel+'</a>');
	   $("#jobModal .job-permalink").attr('href',modalData.permalink);
	   $("#jobModal").modal();
	}
</script>
