<?php require_once('header.php'); ?>
<div class="row blog-list-container">
    <div class="left-div col-lg-8 col-md-7 col-sm-12">
        <h3 class="section-title">
            <span>
                <a href="<?php bloginfo('url')?>/blog">
                    Blog.
                </a>
            </span>                  
            <div class="bg-title"></div>
        </h3>
    	<?php if ( have_posts() ) : ?>
	        
	        <div id="main-content" class="row blog-list-row">
	        	<h2 class="rss col-md-12">
						<?php if ( is_day() ) : ?>
							<?php printf( 'Daily Archives: %s', '<span>' . get_the_date() . '</span>' ); ?>
						<?php elseif ( is_month() ) : ?>
							<?php printf( 'Monthly Archives: %s', '<span>' . get_the_date(  'F Y', 'monthly archives date format' ) . '</span>' ); ?>
						<?php elseif ( is_year() ) : ?>
							<?php printf(  'Yearly Archives: %s', '<span>' . get_the_date( 'Y', 'yearly archives date format' ) . '</span>' ); ?>
						<?php elseif( is_category()): ?>
							Topic Archives: <span><?= single_cat_title( '', false ) ?></span>
						<?php elseif( is_tag()): ?>
							Tag Archives: <span><?= single_tag_title( '', false ) ?></span>
						<?php else : ?>
							Blog Archives
						<?php endif; ?>
				</h2>
	            <?php get_template_part('loop'); ?>
	        </div>
	    <?php else : ?>
			<h1>No Posts</h1>	
		<?php endif; ?>

    </div>


	<div class="col-md-4">
		<?php
		// if blog, show blog sidebar  
		if (get_post_type() == 'post' && is_active_sidebar('blog-sidebar')):
			?>
			<div id="sidebar-blog" class="sidebar section">
				<ul>
					<?php dynamic_sidebar('blog-sidebar'); ?>
				</ul>
			</div>
			<?php
		endif;
		?>
	</div>
</div>
<?php require_once('footer.php'); ?>