<?php require_once('header.php'); ?>
<div class="row blog-list-container">
    <div class="left-div col-lg-8 col-md-7 col-sm-12">
    	
        <div id="main-content" class="row blog-list-row">
			<?php 
				get_template_part('single-blog');
			?>
			<div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 clearfix">
				<?php comments_template( '', true );  ?>
			</div>
        </div>

    </div>

	<div class="col-md-4 col-sm-12 col-xs-12">
		<?php
		// if blog, show blog sidebar  
		if (get_post_type() == 'post' && is_active_sidebar('blog-sidebar')):
			?>
			<div id="sidebar-blog" class="sidebar section">
				<ul>
					<?php dynamic_sidebar('blog-sidebar'); ?>
				</ul>
			</div>
			<?php
		endif;
		?>
	</div>
</div>
<?php require_once('footer.php'); ?>
