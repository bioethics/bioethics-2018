<?php

    // default iamge URL
    $image_url = get_template_directory_uri().'/images/placeholder.png';

    //Check if image is present in content
    preg_match('@(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^"&\n]+|(?<=(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+@', get_the_content(), $videosArray);

    // check for the videos
    if (!empty($videosArray) && (strlen(trim($videosArray[0])) == 11)) {
        $src = array_pop($videosArray);
        $src = 'https://www.youtube.com/embed/'.$src;

        set_query_var( 'src', $src );
        get_template_part('partials/feature-video');
    } else {
        //Check if image is present in content
        preg_match_all('@<img.+?src="([^"]+)"@', get_the_content(), $imageArray);

        if (!empty($imageArray)) {
            $src = array_pop($imageArray);

            if(!empty($src[0])) {
                $image_url = $src[0];
            };
        }

        set_query_var( 'image_url', $image_url );
        get_template_part('partials/feature-img');
    }


?>
