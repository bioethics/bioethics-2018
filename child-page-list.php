<?php
/**
 * Template Name: About - Child Page List
 *
 */

require_once('header.php'); ?>

<h2 class="title"><?php the_title(); ?></h2>
<div id="child-list" class="about-content">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div class="entry">
			<?php the_content(); ?>
			<?php edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>	
		</div><!--end entry-->
	<?php 
	$my_menu = wp_nav_menu(array(
		'theme_location' => 'about_nav',
		'container' => 'div',
		'container_id' => 'about-nav',
		'echo' => 0
	));
	endwhile; ?>
<?php 
endif;	
	$children = query_posts('post_type=page&post_parent='.$post->ID."&orderby=menu_order&order=ASC");
	if(count($children) > 0):
		$i=1;
		foreach($children as $c): 
			setup_postdata($c);
			$children = get_children($c->ID);
			// $permalink = get_permalink($c->ID);
 ?>
			<div class="list-item entry">
				<h3 data-id="<?php echo $i ?>" class="list-item-title">
					<?= $c->post_title; ?>
					<span class="permalink">(<a href="<?= get_permalink($c->ID) ?> " title="Permanent link for <?= $c->post_title; ?> ">Permalink</a>)</span>
				</h3>
				
				<div data-id="<?php echo $i ?>" class="list-item-text" style="display:none">
					<?= apply_filters('the_content', $c->post_content); ?>
					<?php 
						if(count($children) > 0): 
							$x = 0;
							foreach($children as $c):?>
								<h4 data-id="<?= $i.'-'.$x ?>" class="list-item-title">
								<?= $c->post_title; ?>
								<span class="permalink">(<a href="<?= get_permalink($c->ID) ?> " title="Permanent link for <?= $c->post_title; ?> ">Permalink</a>)</span>
								</h4>
								<div data-id="<?= $i.'-'.$x ?>" class="list-item-text" style="display:none;">
									<?= apply_filters('the_content', $c->post_content); ?>
								</div>
					<?php	$x++;
						endforeach;
					endif;
					?>
				</div>
			</div>
<?php	
			$i++;
		endforeach;
	endif;	
	wp_reset_postdata();
 ?>
</div>
<div id="sidebar-about" class="sidebar section">
	<?php
	echo $my_menu;
	?>
</div>

<?php require_once('footer.php'); ?>