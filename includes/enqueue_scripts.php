<?php
/**
 * Enqueue Styles and Scripts
 * Prints css and javascript links
 * @uses wp_enqueue_scripts action hook
*/

if ( ! function_exists( 'bioethics_enqueue_scripts' ) ) :

function bioethics_enqueue_scripts() {
	if ( ! is_admin() ) {
	
		// Main Stylesheet
		wp_enqueue_style(
			'bioethics-style',
			get_bloginfo( 'stylesheet_url' )
		);
		
		// Modernizr
		wp_enqueue_script(
			'modernizr',
			get_template_directory_uri() . 
			'/includes/js/modernizr.min.js'
		);
		
		// Deregister local jQuery and replace with latest Google CDN
		wp_deregister_script( 'jquery' );
		wp_register_script(
			'jquery',
			get_template_directory_uri().'/vendor/jquery/3.3/jquery.min.js'
		);
		wp_enqueue_script( 'jquery' );

		// Theme Javascript
		wp_enqueue_script(
			'theme-script',
			get_template_directory_uri() . '/javascripts/theme.js',
			'', 
			'', 
			true # in footer
		);

		// Theme Javascript
		wp_enqueue_script(
			'bootstrap-script',
			get_template_directory_uri(). '/vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js',
			'', 
			'', 
			true # in footer
		);

		// Theme Javascript
		wp_enqueue_script(
			'ise-viewport',
			get_template_directory_uri(). '/js/ie10-viewport-bug-workaround.js',
			'',
			'',
			true # in footer
		);

		wp_enqueue_script(
			'twitter-mobile',
			get_template_directory_uri(). '/includes/js/twitter-mobile-feeds.js',
			'',
			'',
			true # in footer
		);
		//add tinymce js to frontend ONLY if page has form.
		// add_action("gform_enqueue_scripts", "enqueue_custom_script", 10, 2);
		// function enqueue_custom_script($form, $is_ajax){			
		// 	wp_enqueue_script( 
		// 		'my_tiny_mce',
		// 		get_bloginfo('url').'/wp-includes/js/tinymce/tiny_mce.js'
		// 	);
		// }
	}

}

function bioethics_admin_enqueue_scripts(){
	wp_enqueue_script(
		'admin-script',
		get_template_directory_uri() . '/javascripts/admin.js',
		array('jquery'), 
		'1', 
		true # in footer
	);
}
endif;


function slider_enqueue_scripts() {

	if( is_front_page() ) {
		wp_enqueue_script(
			'slick-script',
			get_template_directory_uri(). '/vendor/slick-1.8.0/slick/slick.min.js',
			'', 
			'', 
			true # in footer
		);	

		// Slider Javascript
		wp_enqueue_script(
			'jssor-script',
			get_template_directory_uri(). '/js/jssor.slider.min.js',
			'', 
			'', 
			true # in footer
		);	

		// Slider Javascript
		wp_enqueue_script(
			'slider-js-script',
			get_template_directory_uri(). '/js/slider.js',
			'', 
			'', 
			true # in footer
		);

		// Slick css
		wp_enqueue_style(
			'slick',
			get_template_directory_uri(). '/vendor/slick-1.8.0/slick/slick.css',
			'', 
			'', 
			false # in footer
		);

		// Slick theme css
		wp_enqueue_style(
			'slick-theme-style',
			get_template_directory_uri(). '/vendor/slick-1.8.0/slick/slick-theme.css',
			'', 
			'', 
			false # in footer
		);
	}
}



add_action( 'wp_enqueue_scripts', 'bioethics_enqueue_scripts' );
add_action( 'admin_enqueue_scripts', 'bioethics_admin_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'slider_enqueue_scripts' );