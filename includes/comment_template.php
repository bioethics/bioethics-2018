<?php
/**
 * Comment Functions
 * Builds comment_template function
*/


# Comments Template
if ( ! function_exists( 'render_comment' ) ) :

	function render_comment( $comment, $args, $depth ) {
	
		$GLOBALS['comment'] = $comment;
		
		switch ( $comment->comment_type ) {
			case ""; ?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
			
				<article id="comment-<?php comment_ID(); ?>" class="comment">
				
					<header class="comment-header">
					
						<span class="vcard">
						
							<?php echo get_avatar( $comment, 96 );
							printf( sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
							
						</span><!--end .vcard -->
						<?php if ( $comment->comment_approved == '0' ) : ?>
						
						<em><?php _e( 'Your comment is awaiting moderation.', 'render' ); ?></em>
						<?php endif; ?>
						<div class="comment-meta">
						
							<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
								<time class="comment-time" pubdate datetime="<?php comment_time( 'c' ); ?>">
									<?php printf( __( '%1$s at %2$s', 'render' ), get_comment_date(), get_comment_time() ); ?>
								</time>
							</a>
							
						</div><!--end .comment-meta -->
						
					</header><!--end .comment-header -->
					<div class="comment-content .format">
					
						<?php comment_text(); ?>								
					
					</div>
					
					<button class="reply">
					
						<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args[ 'max_depth' ] ) ) ); ?>
						
					</button><!--end .reply -->
			
				</article><!--end .comment -->
				
					<?php
    		break;
			case "pingback";
			case "trackback"; ?>
			
<li class="post pingback">
<p><?php _e( 'Pingback:', 'render' ); comment_author_link(); edit_comment_link( __( '(Edit)', 'render' ), ' ' ); ?></p>
<?php
         		break;
		}
	}
endif;

# Comment Form 
function render_comment_form()	{

	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$commenter = wp_get_current_commenter();
	
	$fields =  array(
		'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'render' ) .
					  ( $req ? ' <span class="required">*</span>' : '' ) .
			        '</label><input id="author" name="author" type="text" value="' .
			         esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
		'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'render' ) .
					 ( $req ? ' <span class="required">*</span>' : '' ) .
			        '</label><input id="email" name="email" type="text" value="' .
			         esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
		'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website', 'render' ) .
					 '</label><input id="url" name="url" type="text" value="' .
					 esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
	);

	$args =	array(
		'fields' => $fields,
		'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'render' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
		'',
		'',
		'',
		'comment_notes_after' => '',
		'',
		'',
		'title_reply' => __( 'Leave a Comment', 'render' ),
		'',
		'',
		'label_submit' => 'Submit Comment'
	);

	comment_form($args);

}
