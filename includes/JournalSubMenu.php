<?php
class subMenu extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
        if ('Journals' == $item->title) {
            $output .= sprintf( "\n<li><a href='%s'%s>%s&nbsp;<span class='caret'></span></a>\n",
                'JavaScript:void(0)',
                ' data-toggle="collapse" data-target="#journalMobile"',
                $item->title
            );
        } else {
            $output .= sprintf( "\n<li><a href='%s'>%s</a>\n",
                $item->url,
                $item->title
            );
        }
    }

    function end_el(&$output, $item, $depth=0, $args=array()) {

        if ('Journals' == $item->title) {
                $nav_editions = $args->walker_arg;
                $i = 1;
                $c = count($nav_editions);

                $output .= '<ul id="journalMobile" class="collapse">';

                foreach ($nav_editions as $edition):
                    $current_issue = "https://www.tandfonline.com/openurl?genre=journal&issn=".$edition->meta['isbn']."&volume=".$edition->meta['volume']."&issue=".$edition->meta['number'];
                    $past_issue = "https://www.tandfonline.com/openurl?genre=journal&issn=".$edition->meta['isbn'];

                    $output .= "<li>
                                <div class='featured-description'>
                                    <h5>".$edition->edition->name."</h5>
                                    <h5><a href='".$current_issue."' target='_blank' class='current'>VOL. ".$edition->meta['volume']."  No. ".$edition->meta['number'] ." | ". date('F Y', strtotime($edition->meta['publish_date'])) ."</a></h5>
                                    <h5><a href='$current_issue' target='_blank' class='current'>Current Issue</a> | 
                                        <a href='$past_issue' target='_blank' class='past'>Past Issues</a></h5>
                                </div>
                            </li>";

                        if ($i >= 3) break;
                    $i++;
                endforeach;

                $output .= '</ul>';
        } 
        $output .= "</li>\n";  
    }
}
