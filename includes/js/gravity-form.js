jQuery(document).ready(function($) {
    document.getElementById("gravity-form-cancel").onclick = gravity_cancel_button;
    var createAllErrors = function() {
        var form = $( this );

        var showAllErrorMessages = function() {
            // errorList.empty();
            $('.validation_message').empty();
            $(".gform_fields li").removeClass("gfield_error");

            // Find all invalid fields within the form.
            var invalidFields = form.find( ":invalid" ).each( function( index, node ) {

                $(node).parent().parent().addClass('gfield_error');
                // Find the field's corresponding label
                var label = $( "label[for=" + node.id + "] "),
                    // Opera incorrectly does not fill the validationMessage property.
                    // message = node.validationMessage || 'Invalid value.';
                    message = 'Please enter '+label.html();
                    $(node).parent().after("<div class='gfield_description validation_message'>" + message + "</div>");
            });
        };

        // Support Safari
        form.on( "submit", function( event ) {
            if ( this.checkValidity && !this.checkValidity() ) {
                $( this ).find( ":invalid" ).first().focus();
                event.preventDefault();
            }
        });

        $( "input[type=submit], button:not([type=button])", form )
            .on( "click", showAllErrorMessages);

        $( "input", form ).on( "keypress", function( event ) {
            var type = $( this ).attr( "type" );
            if ( /date|email|month|number|search|tel|text|time|url|week/.test ( type )
              && event.keyCode == 13 ) {
                showAllErrorMessages();
            }
        });
    };
    
    $( "form" ).each( createAllErrors );

    function gravity_cancel_button(){
        if($('#gform_wrapper_1'). length){
            location.pathname = '/jobs/';
        }else{
            location.pathname = '/events/';
        }
    }
});