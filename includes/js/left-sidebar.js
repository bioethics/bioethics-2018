jQuery(document).ready(function($) {
    $(document).click(function(event) {
       if ($(event.target).closest(".resources-main .fa-bars").hasClass('resource-menu-icon')) {
           //click is inside the menu so open menu
          $("body").toggleClass("resources-menu-toggle");
       }else{
           //click is outside menu so check if it is open and close it
           if($("body").hasClass("resources-menu-toggle")){
            $("body").removeClass("resources-menu-toggle");
           }
       }
    });

    $('#about-nav li.current-menu-item').children().css('color','#000');
});