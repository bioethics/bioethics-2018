
jQuery(document).ready(function($) {

    $('.mobile-logo-anchor').after('<i id="showRight" class="fa fa-calendar"></i>');
    $(".navbar-toggler").on("click", function() {
        if ($("#navbarResponsive").hasClass("in")) {
            $("#navbarResponsive").collapse("hide");
        } else {
            $("#navbarResponsive").collapse("show");
        }
    });


    var showRight = document.getElementById('showRight');
    var menuRight = document.getElementById('cbp-spmenu-s2');
    var hideRight = document.getElementById('hideRight');
    showRight.onclick = function() {
        classie.toggle(this, 'active');
        classie.toggle(menuRight, 'cbp-spmenu-open');
    };
    hideRight.onclick = function() {
        classie.toggle(this, 'active');
        classie.toggle(menuRight, 'cbp-spmenu-open');
    };
    var bodyWidth = document.body.clientWidth;
    if (bodyWidth < 970) {
        $("#sideMenu").appendTo("#cbp-spmenu-s2");
    } else {
        $("#sideMenu").appendTo("#sideMenuDesktop");
    }

    $(window).resize(function() {
        var bodyWidth = document.body.clientWidth;
        if (bodyWidth < 970) {
            $("#sideMenu").appendTo("#cbp-spmenu-s2");
        } else {
            $("#sideMenu").appendTo("#sideMenuDesktop");
            if ($("#cbp-spmenu-s2").hasClass("cbp-spmenu-open")) {
                classie.toggle(menuRight, 'cbp-spmenu-open');
            }
        }
    });

    $('body').click(function(event) {
        if (!($(event.target).closest("#cbp-spmenu-s2").length) && !($(event.target).closest("#showRight").length)) {
            if ($("#cbp-spmenu-s2").hasClass("cbp-spmenu-open")) {
                classie.toggle(menuRight, 'cbp-spmenu-open');
            }
        }
    });
});