<?php require_once('header.php'); ?>

<div class="row job-details-container">
            <div class="left-div col-lg-10 col-md-8 col-sm-12">

                <h3 class="section-title">
                    <span>Bioethics Events</span>
                    <div class="bg-title"></div>
                </h3>
                <?php if (have_posts()) :

                while (have_posts()) : the_post();
					$start = get_field('start_date');
					$end = get_field('end_date');
					$city =	get_field('city');
					$state = get_field('state');
					$province = get_field('province');
					$country = get_field('country');
					$website = get_field('website');
					$contact = get_field('contact_name');
					$tele = get_field('telephone');
					$email = get_field('email');


					$location = array();
					if($city && $city !='') $location[] = $city;
					if($state && $state !='') $location[] = $state;
					if($province && $province!='') $location[] = $province;
					if($country && $country !='') $location[] = $country;
                ?>
	                <div class="row job-details-row">
	                    <div class="col-xs-12">

	                        <b>
								<small>
									<?php echo date('l, F jS, Y',strtotime(get_field('start_date')));
									if($end && $end != $start) echo " - ".date('l, F jS',strtotime($end));?>
								</small>
							</b>
	                        <table class="table">
	                            <tbody>
	                                <tr>
	                                    <td>Event Tile:</td>
	                                    <td><strong><?php the_title(); ?></strong></td>
	                                </tr>
	                                <tr>
	                                    <td>LOCATION:</td>
	                                    <td><?php echo implode(', ',$location);?></td>
	                                </tr>
	                                <?php if($website): ?>
										<tr>
		                                    <td>WEBSITE:</td>
											<td>
												<a href="<?php echo $website; ?>"><?php echo $website; ?></a>
											</td>
		                                </tr>
									<?php endif;

									if($contact): ?>
										<tr>
	                                    <td>CONTACT:</td>
	                                    <td><?php echo $contact; ?></td>
	                                </tr>
									<?php endif;

	                                if($email): ?>
										<tr>
		                                    <td>EMAIL:</td>
		                                    <td>
		                                        <a href="mailto:<?php echo $email;?>"><?php echo $email; ?></a>
		                                    </td>
		                                </tr>
									<?php endif;

	                                if($tele): ?>
										<tr>
		                                    <td>TELEPHONE:</td>
		                                    <td>
		                                        <a href="tel:<?php echo $tele;?>"><?php echo $tele; ?></a>
		                                    </td>
		                                </tr>
									<?php endif; ?>
	                            </tbody>
	                        </table>
	                        <div><b>DETAILS:</b></div>
	                        <div class="event-details">
	                            <?php the_content(); ?>
	                        </div>

<p><?php edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?></p>
	                    </div>

	                </div>
                <?php endwhile; ?>
                <!-- <div id="pagination">
					<?php if(is_single()): ?>
						<span class="nav-new">
							<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
						</span>
						<span class="nav-old">
							<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
						</span>
					<?php endif; ?>
				</div> -->
	<!-- /#pagination-->
				<?php else : ?>
					<h1>No Posts!</h1>
				<?php endif; ?>
            </div>


        </div>

<?php
require_once('footer.php');