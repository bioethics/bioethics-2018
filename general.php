<?php 
/**
 * Template Name: General
 *
 */

require_once('header.php'); ?>
<div class="row">
	<div class="col-md-4 resources-menu">
		<?php
		wp_nav_menu(array(
			'theme_location' => 'about_nav',
			'menu_id' => 'about-nav',
		));
		?>

	</div>
	<div class="col-md-8 resources-main">
		<h3 class="section-title">
            <i class="resource-menu-icon fa fa-bars hidden-lg hidden-md"></i><?php the_title(); ?>
            <div class="bg-title"></div>
        </h3>
        <div class="entry">
        	<?php 	
        		if (have_posts()) :
		        	while (have_posts()) : the_post();
		        	 	the_content();
						edit_post_link('Edit this', ' <span class="edit-link">', '</span>');
						endwhile;
				endif;
			?>
		</div>
	</div>
</div>
<?php require_once('footer.php');