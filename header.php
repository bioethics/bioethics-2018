<?php
$template_base = get_template_directory_uri();
$nav_editions = get_new_journals();
?><!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes('html') ?>> <!--<![endif]-->
	<head>
		<title><?php
/*
 * Print the <title> tag based on what is being viewed.
 */
global $page, $paged;

if($post_type=='issues'):
	$fields = get_fields();
	$tax = get_the_terms(get_the_ID(),'editions');
	foreach($tax as $t):
		echo $t->name." : Volume ".$fields['volume']." &bull;  Issue ". $fields['number']." | ";
	endforeach;
else:
	wp_title('|', true, 'right');
endif;
// Add the blog name.
bloginfo('name');

// Add the blog description for the home/front page.
$site_description = get_bloginfo('description', 'display');
if ($site_description && ( is_home() || is_front_page() ))
	echo " | $site_description";

// Add a page number if necessary:
if ($paged >= 2 || $page >= 2)
	echo ' | ' . sprintf('Page %s', max($paged, $page));
?></title>
		<link id="media-url" href="<?= $template_base ?>" rel="media-url">
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="alternate" type="application/rss+xml" title="Bioethics.net: Blog Feed" href="<?php bloginfo_rss('rss2_url'); ?>" />
		<link rel="alternate" type="application/rss+xml" title="Bioethics.net: News Feed" href="<?php bloginfo_rss('rss2_url'); ?>?post_type=news" />
		 <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(['cbp-spmenu-push']); ?>>
		<div id="header">
			<div class="mobile-nav hidden-lg hidden-md">
				<div class="mobile-nav-container">
		            <a href="<?php echo home_url(); ?>" title="Bioethics.net - Where the World Finds Bioethics" class="mobile-logo-anchor"><img src="<?php echo get_template_directory_uri(); ?>/images/lg-logo.png" class="mobile-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/tagline.png" class="mobile-logo"></a>

		            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		                <span class="navbar-toggler-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bars.svg" style="height:20px;"></span>
		            </button>
		            <a data-toggle="collapse"  href="javascript:void(0);" data-target="#mobile-search-container">
		            	<i class="fa fa-search"></i>
		            </a>
		            <div class="collapse" id="navbarResponsive">
						<?php
							include('includes/JournalSubMenu.php');

							wp_nav_menu(array(
								'theme_location' => 'primary_nav',
								'container' => 'div',
								'container_id' => 'primary-nav',
								'menu_class'  => '',
								'link_after' => ' <span class="caret"></span>',
								'walker' => new subMenu,
								'walker_arg' => $nav_editions
							));
						?>
		            </div>
		            <div id="mobile-search-container" class="collapse mobile-search-container">
						<?php get_search_form() ?>
		            </div>
		        </div>
		    </div>

			<nav class="navbar navbar-main navbar-expand-lg fixed-top hidden-xs hidden-sm">
		        <div class="container">
		            <div class="navbar-header">
		                <a href="<?php echo home_url(); ?>" title="Bioethics.net - Where the World Finds Bioethics" class="navbar-brand">
		                	<img src="<?php echo get_template_directory_uri(); ?>/images/lg-logo.png"> 
		                	<img src="<?php echo get_template_directory_uri(); ?>/images/tagline.png" class="hidden-md"> 
		               	</a>
		            </div>
		            <div class="collapse in navbar-collapse">
						<?php
						wp_nav_menu(array(
							'theme_location' => 'primary_nav',
							'container' => 'div',
							'container_id' => 'primary-nav',
							'menu_class'  => 'navbar-nav ml-auto'
						));
						?>
		                <ul class="nav navbar-nav navbar-right">
		                    <li style="padding-top: 10px;">
		                    	<?php get_search_form() ?>
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </nav>
		    
			<div id="nav-journals" class="hidden-sm hidden-xs" style="display: none;">
				<div class="close"><a href="">Close</a></div>
				<ul>
					<?php
					$i = 1;
					$c = count($nav_editions);
					foreach ($nav_editions as $edition):
						$journal_fields = get_fields($journal_id);
						?>

						<li id="link-<?= $edition->edition->slug ?>">
							<div class="journal-detail<?php if ($i == $c) echo " last" ?>">
								<a href="https://www.tandfonline.com/openurl?genre=journal&issn=<?=$edition->meta['isbn']?>&volume=<?=$edition->meta['volume']?>&issue=<?=$edition->meta['number'];?>" target="_blank" class="current"><img src="<?= $edition->meta['cover_image']['sizes']['thumbnail']; ?>" alt="VOL. <?= $edition->meta['volume']; ?> No. <?= $edition->meta['number']; ?> | <?= date('F Y', strtotime($edition->meta['publish_date'])); ?>"></a>
								<h5><?= $edition->edition->name; ?></h5>
								<h5>VOL. <?= $edition->meta['volume']; ?> No. <?= $edition->meta['number']; ?> | <?= date('F Y', strtotime($edition->meta['publish_date'])); ?></h5>
			                    <h8>
			                    	<a href="https://www.tandfonline.com/openurl?genre=journal&issn=<?=$edition->meta['isbn']?>&volume=<?=$edition->meta['volume']?>&issue=<?=$edition->meta['number'];?>" target="_blank" class="current">Current Issue</a> | 
									<a href="https://www.tandfonline.com/openurl?genre=journal&issn=<?=$edition->meta['isbn']?>" target="_blank" class="past">Past Issues</a>
								</h8>
								<!-- /.issues -->
							</div>
						</li>

						<?php
						if($i >=3) break;
						$i++;
					endforeach;
					?>
				</ul>
			</div> <!-- /#nav-journals -->

			<div class="top-banner">
				<div class="container">
					<div class="ad-banner">
				        <!-- Ad Butler banner -->
				        <?php if ( is_active_sidebar( 'top-ad-banner' ) ) : ?>
							<?php dynamic_sidebar( 'top-ad-banner' ); ?>
						<?php endif; ?>
					</div>
					<div class="subscribe hidden-sm hidden-xs img-top-subscribe">
						<a href="https://www.tandfonline.com/action/pricing?journalCode=uajb20" target="_blank" title="Subscribe to The American Journal of Bioethics">
							<img src="<?php echo get_template_directory_uri(); ?>/images/subscribe-button-top.png" width="251" height="90" alt="Subscribe to The American Journal of Bioethics" />
						</a>
					</div>
				</div>
		    </div>

		</div> <!-- /#header -->

		<div class="container main-container">
			<div>
		    	<span>
				    <a class="journal-links" href="<?php echo home_url(); ?>/get-published">Get Published</a>
			        <a class="journal-links">|</a>
			        <a class="journal-links" href="<?php echo home_url(); ?>/subscribe">Subscribe</a>
			        <a class="journal-links">|</a>
			        <a class="journal-links" href="<?php echo home_url(); ?>/about">About</a>			        
			        <a class="journal-links">|</a>
			        <a class="journal-links" href="<?php echo home_url(); ?>/write-for-our-blog">Write for Our Blog</a>
		    	</span>&nbsp;&nbsp;&nbsp;
	    	    <span class="follow-us">
					<span id="follow-device">follow us:</span>
                    <a href="/about/rss-feeds"><i class="fa fa-rss"></i></a>
                    <a href="https://twitter.com/bioethics_net" target="_blank" title="Bioethics.net on Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.facebook.com/groups/bioethics.net/" target="_blank" title="Bioethics.net on Facebook"><i class="fa fa-facebook"></i></a>
                </span>
			</div>