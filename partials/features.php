<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <!-- Side Widget OTHER FEATURED BLOG ARTICLES-->
        <div class="card my-4">
            <h5 class="card-header yellow-h">Other Featured Blog Articles</h5>

            <div class="card-body other-blogs">
	            <?php 
					// feature posts w/o main feature
		      		$itemscount = get_field('blog_items_count', get_the_ID());
					$args = array(
						'cat' => '29',
						'post__not_in' => array($feature_id),
						'posts_per_page' => $itemscount
					);
					$p = query_posts($args);
					while (have_posts()) : the_post();
	            ?>
                <div>
                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                    <hr>
                    <div class="date">
                        <?php echo get_the_date()?>
                    </div>
                </div>
               	<?php
					endwhile;
					wp_reset_query();
				?>
            </div>
        </div>

        <!--Advertisement-->
        <div class="advertisement hidden-lg hidden-md">
            <span>Advertisement</span>
            <img src="<?php echo get_template_directory_uri(); ?>/images/add1.jpeg" class="">
        </div>



    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-6" id="sideMenuDesktop">
        <div id="sideMenu">
            <!-- Access Widget -->
            <?php the_widget( 'rtw_twitter_widget' ); ?> 
            
            <!--Advertisement-->
            <div class="advertisement hidden-sm hidden-xs">
                <span>Advertisement</span>
                <img src="<?php echo get_template_directory_uri(); ?>/images/add1.jpeg" class="">
            </div>





            <div class="card hidden-md hidden-sm hidden-xs hidden-lg">
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="video" width="100%" height="250" src="https://www.youtube.com/embed/-h0qnGKYjPY" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>