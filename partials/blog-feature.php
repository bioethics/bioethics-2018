
<h3 class="section-title">
    Blog.
    <div class="btn btn-white"><a class="dd" href="<?php echo get_site_url().'/blog' ?>">See All</a></div>
    <div class="bg-title"></div>
</h3>

<?php
    
    $featurePosts = [];

    // feature posts w/o main feature
    $fVid = get_field('featured_blog_entry');
    if ($fVid) {
        $args = ['p' => $fVid->ID];
    	query_posts($args);
    	while (have_posts()) : the_post();
            get_template_part( 'detect','media' ); 
    	endwhile;
    	wp_reset_query();
    }

    // feature posts w/o main feature
    $f = get_field('featured_blog_with_image');
    if ($f) {
        $args = ['p' => $f->ID];
        query_posts($args);
        while (have_posts()) : the_post();
            get_template_part( 'detect','media' ); 
        endwhile;
        wp_reset_query();
    }

?>