<!-- News section started -->
<?php 
    $jobs = get_current_jobs(false, 12);
    // Show remaining sliders as blank
    $fillerSliders = 4 - ($jobs->post_count % 4);

    if ( $jobs) : 
?>
<div id="jobs" class="section">
	<h3 class="section-title">
		Jobs.
        <div class="btn btn-white"><a href="<?php echo get_site_url() ?>/jobs">See All</a></div>
        <div class="bg-title"></div>
   	</h3>
        
        <!--LG-->
    <div class="jobs-slider">

    	<?php 
			while ( $jobs->have_posts() ) : $jobs->the_post(); 
                $deadline = get_field('application_deadline');
                $deadline_text = get_field('application_deadline_text');

                $location = [];
                $city = get_field('city');
                if($city && $city !='') $location[] = $city;
                $state = get_field('state');
                if($state && $state !='') $location[] = $state;
                $province = get_field('province');
                if($province && $province!='') $location[] = $province;
                $country = get_field('country');
                if($country && $country !='') $location[] = $country;
		?>

			<div class="slick-single-slide">
				<div class="card">
                    <h5 class="card-header"><a href="<?php the_permalink() ?>" title="<?= the_title() ?>" class="black-text"><?php echo substr(get_the_title(),0,60); ?></a></h5>
                    <div class="card-body">
                        <?php the_field('excerpt') ?>
                        <p class="text-blue"><?php the_field('institution_name') ?></p>
                        <p>
                            <?php echo implode(', ',$location); ?>
                        </p>
                        <p>
                            <?php echo custom_excerpt(get_field('short_description'), 10); ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <b>By: <?= ($deadline_text!='') ? $deadline_text : date('F jS, Y',  strtotime($deadline)) ?></b>
                        <span class="details">
                            <a href="<?php the_permalink() ?>" class="btn btn-primary" title="View more job info for <?php the_title() ?>">Details</a>
                        </span>
                    </div>
                </div>
            </div>
        <?php
        	//endif;
        	++$i;
        	endwhile;
			wp_reset_query();
        ?>  
    </div>
</div>
<?php endif; ?>
<!-- /#jobs.section -->