<h3 class="section-title">
    Journals.
    <div class="bg-title"></div>
</h3>
<div id="journalCarousel" class="carousel slide" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    	<?php
			$i = 1;
			$c = count($nav_editions);
			foreach ($nav_editions as $edition):
		?>
	        <div class="item <?php if($i == 1) echo 'active'?> ">
	            <div class="journal <?php if ($i == $c) echo " last" ?>">
	                <div class="text-center journal-cover">
	                    <a href="https://www.tandfonline.com/openurl?genre=journal&issn=<?=$edition->meta['isbn']?>&volume=<?=$edition->meta['volume']?>&issue=<?=$edition->meta['number'];?>" target="_blank"><img src="<?= $edition->meta['cover_image']['sizes']['journal_medium'];?>" alt="No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?>"></a>
	                </div>
	                <div class="featured-description">
	                    <h5><?= $edition->edition->name ?></h5>
	                    <h5>VOL. <?= $edition->meta['volume'] ?> No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?></h5>
	                    <h8>
	                    	<a href="https://www.tandfonline.com/openurl?genre=journal&issn=<?=$edition->meta['isbn']?>&volume=<?=$edition->meta['volume']?>&issue=<?=$edition->meta['number'];?>" target="_blank" class="current">Current Issue</a> | 
							<a href="https://www.tandfonline.com/openurl?genre=journal&issn=<?=$edition->meta['isbn']?>" target="_blank" class="past">Past Issues</a>
						</h8>
	                </div>
	            </div>
	        </div>
	        <!-- End Item -->

		<?php
				// Break after 3
				if ($i == 3) break;
			$i++;
			endforeach;
		?>
    </div>
    <!-- End Carousel Inner -->


    <ul class="list-group col-sm-4">
    	<?php
			$i = 1;
			foreach ($nav_editions as $edition):
		?>

        <li data-target="#journalCarousel" data-slide-to="<?php echo($i - 1) ?>" class="list-group-item <?php if($i == 1) echo 'active'?>">
            <h4><?= $edition->edition->name ?></h4></li>
        <?php
	        // Break after 3
	        if ($i == 3) break;
			$i++;
			endforeach;
		?>
    </ul>

    <!-- Controls -->
    <div class="carousel-controls">
        <a class="left carousel-control" href="#journalCarousel" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="right carousel-control" href="#journalCarousel" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </div>

</div>
<!-- End Carousel -->