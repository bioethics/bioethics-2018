<?php

$args = [
    'posts_per_page' => 4,
    'post_type'      => 'post',
    'category__not_in' => [29],
    'orderby' => 'publish_date',
    'order' => 'DESC',
    'post_status' => 'publish',
    'post__not_in' => $featurePosts,
    'tag__not_in' => [1747]
];

$query = new WP_Query( $args );

// Check that we have query results.
if ( $query->have_posts() ) :
    while ($query->have_posts()) : $query->the_post();  
?> 
    <div class="slick-single-slide">
        <!-- Blog Post -->
        <h5 class="title"><a title="<?php the_title(); ?>" href="<?php the_permalink() ?>"><?php echo custom_excerpt(get_the_title(), 10); ?></a></h5>
        <div class="card mb-4">
            <div class="card-body">

                <p class="card-text">
                    <?php 
                        echo custom_excerpt(get_the_content(), 40);
                    ?><br/>
                    <a href="<?php the_permalink() ?>" class="btn btn-primary">Read More →</a>
                </p>
            </div>
            <div class="card-footer text-muted">
                Posted on <?php echo  get_the_date("F d, Y") ?>
                <?php social_share_icons(); ?>
            </div>
        </div>
    </div>
<?php
endwhile;
endif;
wp_reset_postdata();
?>