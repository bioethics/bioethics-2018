<div id="journalCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Wrapper for slides -->
    <div class="carousel-inner">
		<?php
		$i = 1;
		$c = count($nav_editions);
		foreach ($nav_editions as $edition):
		?>

			<div class="item">
				<div class="journal">
					<div class="text-center journal-cover">
						<a href="<?= get_permalink($edition->ID); ?>"><img src="<?= $edition->meta['cover_image']['sizes']['journal_medium'];?>" alt="No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?>"></a>
					</div>
					<div class="featured-description">
						<h5><?= $edition->edition->name ?></h5>
						<h5>VOL. <?= $edition->meta['volume'] ?> No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?></h5>
						<h8>
							<a href="<?= get_permalink($edition->ID); ?>" class="current">Current Issue</a>
							<a href="<?= get_bloginfo('url') . '/editions/' . $edition->edition->slug ?>" class="past">Past Issues</a>
							<!-- <li><a href="/about/editors/" class="editors">The Editors</a></li> -->
						</h8>
					</div>
					<!-- /.featured-description -->
				</div>
				<!-- /.journal -->
			</div>
			<!-- /.item -->

		<?php
		$i++;
		endforeach;
		?>
	</div>
	<!-- End Carousel Inner -->
	
	<ul class="list-group col-sm-4">
		<?php
		$i = 1;
		foreach ($nav_editions as $edition):
			?>

			<li data-target="#journalCarousel"  data-slide-to="<?php echo($i - 1) ?>" class="list-group-item" id="j-item<?= $i ?>">
				<h4><?= $edition->edition->name ?></h4>
			</li>
			<!-- /.journal-item-link -->

			<?php
			$i++;
		endforeach;
		?>
	</ul>
	<!-- /#journal-list -->
</div>
<!-- /#journal-feature.section -->