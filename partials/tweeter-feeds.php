<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <!-- Side Widget OTHER FEATURED BLOG ARTICLES-->
        <div class="card my-4">
            <h5 class="card-header yellow-h">Other Featured Blog Articles</h5>

            <div class="card-body other-blogs">
	            <?php 
					// feature posts w/o main feature
		      		$itemscount = get_field('blog_items_count', get_the_ID());
					$args = array(
						'cat' => '29',
						'post__not_in' => $featurePosts,
						'posts_per_page' => $itemscount
					);
					$p = query_posts($args);
					while (have_posts()) : the_post();
	            ?>
                <div>
                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                    <hr>
                    <div class="date">
                        <?php echo get_the_date()?>
                    </div>
                </div>
               	<?php
					endwhile;
					wp_reset_query();
				?>
            </div>
        </div>
    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-6 col-sm-12 col-xs-12">
        <!-- Access Widget -->
        <div>
            <div class="card twitter-card">
                <h5 class="card-header yellow-h">Twitter Feeds</h5>
                <?php echo do_shortcode("[custom-twitter-feeds]") ?>
            </div>
        </div>
        <br/>

        <!--Advertisement-->
        <div id="sidebarAd">
            <?php if ( is_active_sidebar( 'rightside-ad-banner' ) ) : ?>
                <?php dynamic_sidebar( 'rightside-ad-banner' ); ?>
            <?php endif; ?>
        </div>
    </div>
</div>