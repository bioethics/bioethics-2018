<!-- News section started -->
<div id="news" class="section">
	<h3 class="section-title">
		Bioethics news.
		<a href="/news/feed" title="Bioethics News RSS Feed">
			<img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Btn Rss">
		</a>
        <div class="btn btn-white"><a href="<?php echo get_site_url() ?>/news">See All</a></div>
        <div class="bg-title"></div>
   	</h3>
        

    <div class="news-slider">

        <!-- Slides Container -->
    	<?php 
        	$news_total = get_field('news_items_count', get_the_ID());
	       // feature posts w/o main feature
			$args = array(
				'post_type' => 'news',
				'posts_per_page' => 12,
			);
			query_posts($args);
			$i = 1;
			while (have_posts()) : the_post();

		?>
			<div class="slick-single-slide">
				<div class="card">
                    <h5 class="card-header"><a href="<?php the_field('url') ?>" title="<?= the_title() ?>" class="black-text" target="_blank"><?php echo custom_excerpt(get_the_title(), 20); ?></a></h5>
                    <div class="card-body">
                        <?php echo custom_excerpt(get_field('excerpt'), 40); ?>
                    </div>
                    <div class="card-footer text-muted">
                        <i><?php the_field('source') ?></i>,
                        <?php echo get_the_date(); ?>
                    </div>
                </div>
            </div>
        <?php
        	//endif;
        	++$i;
        	endwhile;
			wp_reset_query();
        ?>
    </div>
</div>
<div class="clear-both"></div>
<!-- /#news.section -->