<!-- Image featured Post -->
<h5 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
<div class="card mb-4">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <img class="cover" src="<?php echo $image_url; ?>" alt="Bioethics">
            </div>
            <div class="col-md-9 post-details">
                <i><?php the_author(); ?></i>
            </div>
        </div>

        <p class="card-text">
            <?php 
/*                $content = the_advanced_excerpt('exclude_tags=img,iframe', true); 
                $content = preg_replace('/<img[^>]+\>/i', '',$content);
                echo $content;*/
                the_advanced_excerpt('length=60&length_type=words&no_custom=1&ellipsis=%26hellip;&exclude_tags=img,iframe,p,strong');
            ?>
        </p>
        <a href="<?php the_permalink() ?>" class="btn btn-primary">Read More →</a>
    </div>
    <div class="card-footer text-muted">
        Posted on <?php the_date("F d, Y") ?>
        <?php social_share_icons(); ?>
    </div>
</div>


            