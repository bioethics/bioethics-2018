<!-- Video Post -->
<h5 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
<div class="card mb-4">
    <div class="row">
        <div class="col-md-12">
            <iframe id="video" width="100%" height="300" src="<?= $src; ?>" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="card-body">
        <p class="card-text-date">
            <?php 
/*                $content = the_advanced_excerpt('exclude_tags=img,iframe', true); 
                $content = preg_replace('/<img[^>]+\>/i', '',$content);
                $content = preg_replace('/<iframe[^>]+\>/i', '',$content);
                echo $content;*/
                the_advanced_excerpt('length=100&length_type=words&no_custom=1&ellipsis=%26hellip;&exclude_tags=img,iframe,p,strong');
            ?>
        </p>
        <a href="<?php the_permalink() ?>" class="btn btn-primary">Read More →</a>
    </div>
    <div class="card-footer text-muted">
        Posted on <?php the_date("F d, Y") ?>
        <?php social_share_icons(); ?>
    </div>
</div>
