<div class="list">
	<h4>Other Featured Blog Articles</h4>
	<?php
// feature posts w/o main feature
$itemscount = get_field('blog_items_count', get_the_ID());
	$args = array(
		'cat' => '29',
		'post__not_in' => array($feature_id),
		'posts_per_page' => $itemscount
	);
	$p = query_posts($args);
	while (have_posts()) : the_post();
		?>
		<div class="feature-item">
			<h2 class="date"><?php echo get_the_date()?></h2>
			<h4 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
		</div>
		<!-- /.feature-item -->

		<?php
	endwhile;
	wp_reset_query();
	?>
</div>
<!-- /.list -->