
<h3 class="section-title">
    Blog.
    <div class="btn btn-white">See All</div>
    <div class="bg-title"></div>
</h3>

<?php
	// feature posts w/o main feature
	$f = get_field('featured_blog_entry');
	$feature_id = $f->ID;
	$feature_description = get_field('description');
	query_posts('p=' . $feature_id);
	while (have_posts()) : the_post();
?>
	<!-- Video Post -->
    <h5 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
    <div class="card mb-4">
        <div class="row">
            <div class="col-md-12">
                <iframe id="video" width="100%" height="300" src="https://www.youtube.com/embed/-h0qnGKYjPY" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>
        <div class="card-body">
            <p class="card-text"><?php the_advanced_excerpt('exclude_tags=img'); ?></p>
        </div>
        <div class="card-footer text-muted">
            Posted on <?php the_date(); ?>
            <div class="share">
                <span>Share</span>
                <i class="fa fa-twitter"></i>
                <i class="fa fa-facebook"></i>
                <i class="fa fa-linkedin"></i>
                <i class="fa fa-google-plus"></i>
            </div>
        </div>
    </div>


			
<?php
	endwhile;
	wp_reset_query();
?>