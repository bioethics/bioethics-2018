<?php require_once('header.php'); ?>
<div class="row blog-list-container">
    <div class="left-div col-lg-8 col-md-7 col-sm-12">

        <h3 class="section-title">
            <span>
                <a href="<?php bloginfo('url')?>/blog">
                    Blog.
                </a>
            </span>                   
            <div class="bg-title"></div>
        </h3>
        
        <div id="main-content" class="row blog-list-row blogs-landing">
            <?php get_template_part('loop'); ?>
        </div>

    </div>


	<div class="col-md-4 col-sm-12 col-xs-12">
		<?php
		// if blog, show blog sidebar  
		if (get_post_type() == 'post' && is_active_sidebar('blog-sidebar')):
			?>
			<div id="sidebar-blog" class="sidebar section">
				<ul>
					<?php dynamic_sidebar('blog-sidebar'); ?>
				</ul>
			</div>
			<?php
		endif;
		?>
	</div>
</div>
<?php require_once('footer.php'); ?>