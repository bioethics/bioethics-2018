<?php
global $nav_editions;
require_once('header.php');
?>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right mobile-side-section" id="cbp-spmenu-s2">
    <div id="hideRight">
        <i class="fa fa-chevron-right"></i>
        <img src="<?php echo get_template_directory_uri(); ?>/images/tagline.png">
    </div>
</nav>

<div class="row clear-both top-container">
	<!-- Blog Entries Column -->
    <div class="col-md-6 col-lg-6 col-sm-12">
		<?php include_once('partials/blog-feature.php'); ?>
	</div>
	<div class="col-md-6 col-lg-6 col-sm-12">
		<?php 
			include_once('partials/journal-features.php'); 
			include_once('partials/tweeter-feeds.php');
		?>

	</div>
<!-- /.col -->
</div>
<div class="row" id="recentPosts">
	<?php include_once('partials/recent-posts.php'); ?> 
</div>
<!-- /.feature -->

<div class="row bottom-container">
	<?php 
		include_once('partials/news.php'); 
		include_once('partials/jobs.php');
	?>
</div>

<!-- /#home -->

<?php require_once('footer.php'); ?>
