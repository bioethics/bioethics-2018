<div class="row jobs-rows">
<?php
if ( $jobs) : 
	$lastmonth = null;
	$i = 1;
	$cols = 3;

	if ($jobs->have_posts()):
		while ( $jobs->have_posts() ) : $jobs->the_post(); 
			$deadline = get_field('application_deadline');
			$deadline_text = get_field('application_deadline_text');
		
			//JOBS	
			$location = array();
			$city =	get_field('city');
			if($city && $city !='') $location[] = $city;
			$state = get_field('state');
			if($state && $state !='') $location[] = $state;
			$province = get_field('province');
			if($province && $province!='') $location[] = $province;
			$country = get_field('country');
			if($country && $country !='') $location[] = $country;

	        $jobDetails = json_encode([
	            'title'=> esc_html(get_the_title()),
				'company' => get_field('institution_name'),
				'deadline' => (($deadline_text!='') ? $deadline_text : date('F jS, Y',  strtotime($deadline))),
				'description' => preg_replace("/'/", "&apos;", get_field('long_description')),
				'name' => get_field('name'),
				'tel' => get_field('telephone'),
				'location' => implode(', ',$location),
				'email' => get_field('email'),
				'permalink' => get_permalink(),
				'posted' => get_the_date('l, F jS, Y')
	        ]);
?>

    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 jobs-card-wrapper">
		<div id="job-<?php the_ID(); ?>" <?php post_class('card job-card col-'.$i); ?>>
                <div class="card-header">
                    <span class="job-title"><a href="<?php the_permalink() ?>" title="Bioethics Job: <?php the_field('institution_name') ?> : <?php the_field('title') ?>"><?php echo custom_excerpt(get_the_title(), 8); ?></a></span>
                </div>
                <div class="card-body">
                    Deadline: <?php if($deadline): 
    					echo (($deadline_text!='') ? $deadline_text : date('F jS, Y',  strtotime($deadline)));
    				endif;
                    ?>
                </div>
                <div class="card-footer">
					<div class="btn btn-primary btn-event-details" data-toggle="modal" onclick='showjobsModal(<?= $jobDetails?>)'>Details</div>
                </div>
		</div>
	</div><!--end list-post-->

<?php 
	endwhile; /* rewind or continue if all posts have been fetched */ 

	if (($jobs->found_posts%$cols) != 0)
		echo "</div> <!-- /.row for the last loop -->";

?>
<?php else : ?>
<div class="col-xs-12"><h1>No Posts</h1></div>	
<?php endif; ?>
</div>
<div class="clearfix"></div>
<?php endif; ?>