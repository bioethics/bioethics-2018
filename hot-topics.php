<?php
/**
 * Template Name: Hot Topics
 *
 */

require_once('header.php'); ?>

<div id="child-list">
	<h2 class="title"><?php the_title(); ?>.</h1>
		<?php the_content(); ?>
		<!-- #hot-topics.col -->
		<div class="col split" id="hot-topics">
			<ul>
<?php wp_list_categories('orderby=name&show_count=1&title_li=&echo=false&exclude=210,29&feed_image='.$template_base.'/images/icon_rss_small.gif'); ?>
			</ul>
		</div>
		<!-- /#hot-topics.col -->
</div>

<?php require_once('footer.php'); ?>