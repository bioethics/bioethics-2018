// Copyright 2012 Diamond Merckens Hogan. All Rights Reserved.
// Bioethics.net

/**
* @fileoverview Contains the JavaScript used by the Bioethics site. This file depends on
* the jQuery 1.7 library.
* @author keshleman@diamondmerckenshogan.com (Kevin Eshleman)
*/

var Bioethics = {
	journalSlideShow: null,

	// Initializes the Bioethics web page.
	init: function() {
		
		// Handle when journal link item is clicked.
		$("#primary-nav li.menu-item a:contains('Journal'), #nav-journals div.close").click(function(e) {
			Bioethics.toggleJournalNav(e);
		});
   		
   		// Add class for comment form
   		$('#commentform textarea, #commentform input[type=text]').addClass('form-control');
   		$('#commentform input[type=submit]').addClass('btn btn-primary');
	},

	toggleJournalNav: function(e){
		e.preventDefault();
		$('#nav-journals').slideToggle(500,'swing',function(){
			$('li#menu-item-24331, body').toggleClass('journal-active');
		}); 
	}


};		

// Called after the document loads to initilize the page.
$(document).ready(function($) {
	Bioethics.init();
	/* open or close mobile navigation menu on click */
	$(".navbar-toggler").on("click", function() {
        if ($("#navbarResponsive").hasClass("in")) {
            $("#navbarResponsive").collapse("hide");
        } else {
            $("#navbarResponsive").collapse("show");
        }
    });

	/* close mobile navigation menu on clicking elsewhere */
	$(document).click(function(event) {
	   if (!($(event.target).closest("#navbarResponsive").hasClass("collapse"))) {
		   //click is inside the menu so do nothing
		   if($("#navbarResponsive").hasClass("in")){
			   $("#navbarResponsive").collapse("hide");
		   }
	   }
	});
});