<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-container">
        <div class="search-desktop input-group">
            <input type="text" name="s" id="s" value="<?=get_search_query()?>" class="field form-control" placeholder="Search...">
            <span class="input-group-btn">
                <input type="submit" name="submit" id="searchsubmit" class="btn grey-btn" value="Go!"/>
            </span>
        </div>
    </div>
</form>