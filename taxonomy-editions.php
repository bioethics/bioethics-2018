<?php 
require_once('header.php');
global $paged;
$cat = get_term_by('slug',$editions,'editions');


?>
<div id="list-content">
	<div id="journals-previous">
		<h2 class="title">
		<?php
			if($cat->slug === 'primary_research'){
				echo "AJOB Empirical Bioethics";
			}else{
				echo $cat->name;
			}
					
		?>		
		</h2>
<?php if ( have_posts() ) : 
?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('j-thumb'); ?>>
			<div class="entry">

				
			<?php
//				$img = get_field('cover_image');
				$f = get_fields();
				// var_dump($f['cover_image']);
				$image = $f['cover_image']['sizes']['journal_medium']; ?>
				<a href="<?php the_permalink()?>" class="cover">
					<img src="<?php echo $f['cover_image']['sizes']['journal_medium'];  ?>" alt="<?php echo $f['cover_image']['title'];  ?>" width="141" height="190"/>
                    <h4><?= date('F Y',strtotime($f['publish_date'])); ?></h4>
				</a>
				<div class="topic_list">
				<?php if($f['target_articles']): ?>
					<?php foreach($f['target_articles'] as $article): ?>
						<h5><a href="<?php the_permalink()?>"><?= $article->post_title; ?></a></h5>
					<?php 
					endforeach; 
				endif;
				?>
				</div>
			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; ?>
	<div id="pagination">
		<span class="nav-old">
			<?php next_posts_link('&larr; Older Issues '); ?>
		</span>
		<span class="nav-new">
			<?php previous_posts_link('Newer Issues &rarr;'); ?>
		</span>
	</div><!-- /#pagination-->
	<?php wp_reset_postdata(); ?>
	<?php else : ?>
		<p>No posts found.</p>
	<?php endif; 
?>
	</div><!--end journals-previous-->
</div><!--end content-->

<?php
require_once('footer.php');