<?php
require_once('header.php'); 
if (have_posts()) : 
	while (have_posts()) : the_post();

$info = get_fields();
$terms = wp_get_post_terms(get_the_ID(), 'editions');
$terms = $terms[0];
$terms = single_issue($terms,$info);
$image = $info['cover_image'];
$currentIssueType = $terms->class;
switch($currentIssueType){
    case 'primary_research':
    $img = '/images/bg-journal-header-primary-research.png';
    break;

    case 'empirical_bioethics':
    $img = '/images/bg-journal-header-empirical-bioethics.png';
    break;

    case 'neuroscience':
    $img = '/images/bg-journal-header-neuroscience.png';
    break;

    default:
    $img = '/images/bg-journal-header-ajob.png';
    break;

}
?>

<!-- Page Content -->
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right mobile-side-section" id="cbp-spmenu-s2">
            <div id="hideRight">
                <i class="fa fa-chevron-right"></i>
                <img src="images/tagline.png">
            </div>
        </nav>

        <div class="row clear-both top-container">

            <div class="col-md-8">
                <div class="jumbotron">
                    <div class="journal-header">
                        <img class="img-cover" src="<?php echo $image['sizes']['journal_medium']; ?>" alt="Vol. <?= $info['volume']; ?> No. <?= $info['number']; ?> | <?= date('F Y',strtotime($info['publish_date'])) ?>" />

                        <span class="journal-title text-center">
                            <span class="spacer"></span>
                        <img src="<?php echo get_template_directory_uri().$img;?>">
                        <br>
                        <br>
                        <span class="text-center"><a>Vol. <?= $info['volume']; ?> No. <?= $info['number']; ?> | <?= date('F Y',strtotime($info['publish_date'])) ?></a></span>
                        <br>
                        <span class="text-center"><a>Current Issue </a> | <a>Past Issues</a></span>
                        </span>
                    </div>
                </div>

                <!-- Blog Post -->
                
                <?php 
                if($info['editorial']!=0): 
                ?>
                    <h4 class="title">EDITORIAL</h4>
                    <?php

                    foreach($info['editorial'] as $edi):
                        $edi_info = get_fields($edi->ID);
                        $permalink = get_permalink($edi->ID);
                    ?>
                        <div class="card mb-4 editorial-card">
                            <div class="card-body padded-card">
                                <a href="<?= $permalink; ?>" title="<?=$edi->post_title;?>"><b><?=$edi->post_title;?></b></a>
                                <div><i><?php echo $edi_info['primary_author']; 
                                    if($edi_info['additional_authors']) echo ", ".$edi_info['additional_authors'];?>
                                    </i>
                                </div>

                                <p class="card-text"><?php echo substr(strip_tags($edi_info['abstract']),0,400).'...';?></p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$edi->post_title;?>">Read More →</a>
                            </div>
                            <div class="card-footer text-muted">
                                Posted on <?= date('F d, Y',strtotime($edi->post_date)) ?>
                                <div class="share">
                                    <span>Share</span>
                                    <i class="fa fa-twitter"></i>
                                    <i class="fa fa-facebook"></i>
                                    <i class="fa fa-linkedin"></i>
                                    <i class="fa fa-google-plus"></i>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php 
                endif; 
                ?>


                <!-- Blog Post -->
                <?php if($info['target_articles']!=0): ?>
                    <h4 class="title">Target Article</h4>
                
                <?php
                    $i = 0;
                    foreach($info['target_articles'] as $ta):
                        $ta_info = get_fields($ta->ID);
                        $permalink = get_permalink($ta->ID);
                ?>


                <div class="card mb-4 <?php echo ($i==1)? 'card-grey' : '' ?>">
                    <div class="card-body padded-card">
                        <a href="<?= $permalink; ?>" title="<?=$ta->post_title;?>"><b><?=$ta->post_title;?></b></a>

                        <div>
                            <i><?php echo $ta_info['primary_author']; 
                                if($ta_info['additional_authors']) echo ", ".$ta_info['additional_authors']; ?>
                            </i>
                        </div>
                        <p class="card-text"><?php echo substr(strip_tags($ta_info['abstract']),0,400).'...';?></p>
                        <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$ta->post_title;?>">Read More →</a>
                    </div>
                    <div class="card-footer text-muted">
                        Posted on <?= date('F d, Y',strtotime($ta->post_date)) ?>
                        <div class="share">
                            <span>Share</span>
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-linkedin"></i>
                            <i class="fa fa-google-plus"></i>
                            <i class="fa fa-whatsapp"></i>
                        </div>
                    </div>
                </div>
                <?php   
                    $i++;
                    endforeach;
                endif; // end target articles ?>


                <div id="bottom-section" class="section">

                    <?php if(isset($info['in_focus']) && $info['in_focus'] !=0): $i++;?>
                        <!-- In Focus -->
                        <h4 class="title">In Focus</h4>
                        <?php
                                foreach($info['in_focus'] as $inFocus): 
                                    $inFocusInfo = get_fields($inFocus->ID);
                                    $permalink = get_permalink($inFocus->ID);
                                    ?>
                                <div class="card mb-4">
                                    <div class="card-body padded-card">
                                        
                                        <a href="<?= $permalink; ?>" title="<?=$inFocus->post_title;?>"><b><?=$inFocus->post_title;?></b></a>

                                        <p>
                                            <i><?php echo $inFocusInfo['primary_author']; 
                                                if($inFocusInfo['additional_authors']) echo ", ".$inFocusInfo['additional_authors']; ?>
                                            </i>
                                        </p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$inFocus->post_title;?>">Read More →</a>
                                    </div>
                                </div>
                                <?php   
                        $i++;
                        endforeach;
                    endif; // end In Focus ?>




                    <?php if(isset($info['correspondence']) && $info['correspondence'] !=0): $i++;?>
                        <!-- correspondence -->
                        <h4 class="title">Correspondence</h4>
                        <?php
                                foreach($info['correspondence'] as $correspondence): 
                                    $correspondenceInfo = get_fields($correspondence->ID);
                                    $permalink = get_permalink($correspondence->ID);
                                    ?>
                                <div class="card mb-4">
                                    <div class="card-body padded-card">
                                        
                                        <a href="<?= $permalink; ?>" title="<?=$correspondence->post_title;?>"><b><?=$correspondence->post_title;?></b></a>

                                        <p>
                                            <i><?php echo $correspondenceInfo['primary_author']; 
                                                if($correspondenceInfo['additional_authors']) echo ", ".$correspondenceInfo['additional_authors']; ?>
                                            </i>
                                        </p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$correspondence->post_title;?>">Read More →</a>
                                    </div>
                                </div>
                                <?php   
                        $i++;
                        endforeach;
                    endif; // end correspondence ?>




                    <?php if(isset($info['book_review']) && $info['book_review'] !=0): $i++;?>
                        <!-- Book Review -->
                        <h4 class="title">Book Reviews</h4>
                        <?php
                                foreach($info['book_review'] as $book): 
                                    $bookInfo = get_fields($book->ID);
                                    $permalink = get_permalink($book->ID);
                                    ?>
                                <div class="card mb-4">
                                    <div class="card-body padded-card">
                                        
                                        <a href="<?= $permalink; ?>" title="<?=$book->post_title;?>"><b><?=$book->post_title;?></b></a>

                                        <p>
                                            <i><?php echo $bookInfo['primary_author']; 
                                                if($bookInfo['additional_authors']) echo ", ".$bookInfo['additional_authors']; ?>
                                            </i>
                                        </p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$book->post_title;?>">Read More →</a>
                                    </div>
                                </div>
                                <?php   
                        $i++;
                        endforeach;
                    endif; // end Book Review ?>



                    <?php if(isset($info['column']) && $info['column'] !=0): $i++;?>
                        <!-- Column -->
                        <h4 class="title">Column</h4>
                        <?php
                                foreach($info['column'] as $column): 
                                    $columnInfo = get_fields($column->ID);
                                    $permalink = get_permalink($column->ID);
                                    ?>
                                <div class="card mb-4">
                                    <div class="card-body padded-card">
                                        
                                        <a href="<?= $permalink; ?>" title="<?=$column->post_title;?>"><b><?=$column->post_title;?></b></a>

                                        <p>
                                            <i><?php echo $columnInfo['primary_author']; 
                                                if($columnInfo['additional_authors']) echo ", ".$columnInfo['additional_authors']; ?>
                                            </i>
                                        </p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$column->post_title;?>">Read More →</a>
                                    </div>
                                </div>
                                <?php   
                        $i++;
                        endforeach;
                    endif; // end Column ?>



                    <?php if(isset($info['reflection']) && $info['reflection'] !=0): $i++;?>
                        <!-- reflection -->
                        <h4 class="title">Reflection</h4>
                        <?php
                                foreach($info['reflection'] as $reflection): 
                                    $reflectionInfo = get_fields($reflection->ID);
                                    $permalink = get_permalink($reflection->ID);
                                    ?>
                                <div class="card mb-4">
                                    <div class="card-body padded-card">
                                        
                                        <a href="<?= $permalink; ?>" title="<?=$reflection->post_title;?>"><b><?=$reflection->post_title;?></b></a>

                                        <p>
                                            <i><?php echo $reflectionInfo['primary_author']; 
                                                if($reflectionInfo['additional_authors']) echo ", ".$reflectionInfo['additional_authors']; ?>
                                            </i>
                                        </p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$reflection->post_title;?>">Read More →</a>
                                    </div>
                                </div>
                                <?php   
                        $i++;
                        endforeach;
                    endif; // end reflection ?>



                    <?php if(isset($info['articles']) && $info['articles'] !=0): $i++;?>
                        <!-- Articles -->
                        <h4 class="title">Articles</h4>
                        <?php
                                foreach($info['articles'] as $articles): 
                                    $articlesInfo = get_fields($articles->ID);
                                    $permalink = get_permalink($articles->ID);
                                    ?>
                                <div class="card mb-4">
                                    <div class="card-body padded-card">
                                        
                                        <a href="<?= $permalink; ?>" title="<?=$articles->post_title;?>"><b><?=$articles->post_title;?></b></a>

                                        <p>
                                            <i><?php echo $articlesInfo['primary_author']; 
                                                if($articlesInfo['additional_authors']) echo ", ".$articlesInfo['additional_authors']; ?>
                                            </i>
                                        </p>
                                <a href="<?= $permalink; ?>" class="btn btn-primary" title="<?=$articles->post_title;?>">Read More →</a>
                                    </div>
                                </div>
                                <?php   
                        $i++;
                        endforeach;
                    endif; // end articles ?>     
        
                </div>

                <style>
                    /* jssor slider loading skin spin css */
                    
                    .jssorl-009-spin img {
                        animation-name: jssorl-009-spin;
                        animation-duration: 1.6s;
                        animation-iteration-count: infinite;
                        animation-timing-function: linear;
                    }
                    
                    @keyframes jssorl-009-spin {
                        from {
                            transform: rotate(0deg);
                        }
                        to {
                            transform: rotate(360deg);
                        }
                    }

                </style>

            </div>

            <!-- Sidebar Widgets Column -->
            <?php

            if (get_post_type() == 'issues' && is_active_sidebar('journal-sidebar')):
                    ?>
            <div class="col-md-4" id="sideMenuDesktop">

                  <div id="sidebar-journal" class="sidebar section">
                        <ul>
                            <?php dynamic_sidebar('journal-sidebar'); ?>
                        </ul>
                    </div>
            </div>
            <?php
                endif;
                require_once('footer.php');
                ?>

        </div>
        <!--News and jobs-->

<?php endwhile;
	
else : ?>
	<h1>No Posts!</h1>	
<?php endif; 
	require_once('footer.php'); ?>
