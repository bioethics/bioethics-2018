<?php require_once('header.php');?>

<!-- Modal -->
    <div id="myModal" class="modal fade detail-view-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <b class="event-duration"></b>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>LOCATION:</td>
                                <td class="event-location"></td>
                            </tr>
                            <tr>
                                <td>WEBSITE:</td>
                                <td><a href="" class="event-website"></a></td>
                            </tr>
                            <tr>
                                <td>
                                    CONTACT:
                                </td>
                                <td class="event-contact"></td>
                            </tr>
                            <tr>
                                <td>
                                    EMAIL:
                                </td>
                                <td>
                                    <a href="" class="event-email"></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    TELEPHONE:
                                </td>
                                <td>
                                    <a href="" class="event-telephone"></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div><b>DETAILS:</b></div>
                    <div class="event-details post-details"></div>
                    <a class="event-permalink" href="#">Permanent Link</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right mobile-side-section event-calender-side-section" id="cbp-spmenu-s2">
            <div id="hideRight">
                <i class="fa fa-chevron-right"></i>
                <span>Events Calendar</span>
            </div>
        </nav>
<div class="row events-container">

    <div class="left-div col-lg-8 col-md-7 col-sm-12">
        <h3 class="section-title">
                    Events.
                    <div class="btn btn-white"><a href="<?php bloginfo('wpurl')?>/submit_event" id="subnav-submit">Submit an Event</a></div>
                    <div class="bg-title"></div>
                </h3>
        <?php

        if(have_posts()){
            $lastmonth = null;
            $cols = 4;
            $eventData = array();
            $title = '';
            $increment = 1;
            while ( have_posts() ) : the_post();

                $start      = get_field('start_date');
                $end        = get_field('end_date');
                $website    = get_field('website');
                $contact    = get_field('contact_name');
                $telephone  = get_field('telephone');
                $eventEmail = get_field('email');
                $thismonth  = date('F Y',strtotime($start));

                $title = the_title('','',FALSE);
                $i = 0;
                if($lastmonth==null || $lastmonth != $thismonth):
                    if($i != 1 && $lastmonth != null) echo '</div> <!-- /.row -->';
                    echo '<div class="month-label">'.$thismonth.'</div>';
                    $i = 1;
                endif;
                    
                if($i==1) echo '<div class="row events-row">';?>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 events-card">
                    <div id=<?php echo 'event'.$increment ?> class="card event-card">

                        <div class="card-header">
                            
                            <span class="event-title">
                                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                    <?php echo custom_excerpt(get_the_title(), 20); ?>
                                </a>
                            </span>
                        </div>

                        <?php

                            $location = array();
                            $city = get_field('city');
                            if($city && $city !='') $location[] = $city;
                            $state = get_field('state');
                            if($state && $state !='') $location[] = $state;
                            $province = get_field('province');
                            if($province && $province!='') $location[] = $province;
                            $country = get_field('country');
                            if($country && $country !='') $location[] = $country;

                            $allLocations = implode(', ',$location);

                            $multidayEvents = array();

                            $duration = date('D, M jS Y',strtotime($start));
                            if($end && $end != $start) {
                                $duration .= " - ".date('D, M jS Y',strtotime($end));
                                $period = new DatePeriod(
                                                         new DateTime($start),
                                                         new DateInterval('P1D'),
                                                         new DateTime($end)
                                                    );
                                $dayCount = 1;
                                foreach ($period as $dateObj) {
                                    $multidayEvents[$dayCount] = $dateObj->format('Y-m-d');
                                    $dayCount++;
                                }
                            }
                            $multidayEvents[] = date('Y-m-d',strtotime($end));
                            $modalData = json_encode(array(
                                                    'title'=>esc_html($title),
                                                    'duration'=>esc_html($duration),
                                                    'description'=>htmlspecialchars(esc_html(get_the_content())),
                                                    'permalink'=>esc_url(get_permalink()),
                                                    'location'=>esc_html($allLocations),
                                                    'website'=>$website,
                                                    'contact'=>esc_html($contact),
                                                    'email'=>esc_html($eventEmail),
                                                    'telephone'=>esc_html($telephone)
                                                ));
                            ?>
                        <div class="card-body">
                            <div class="location"><?php echo $allLocations; ?></div>
                            <p><?php the_advanced_excerpt('length=15&allowed_tags=a&finish_sentence=0'); ?><p>
                            <div class='btn btn-primary btn-event-details' data-toggle='modal' onclick='showModal(<?php echo $modalData;?>)'>Details</div>
                        </div>

                        <div class="card-footer">
                            <?php echo $duration; ?>
                        </div>
                    </div>
                    
                    <?php
                    $titleHtml = '<a class="calender-event-title" href="#event'.$increment.'">'.$title.'</a>';
                    foreach ($multidayEvents as $dayCnt => $eventDate) {
                        if($dayCnt) {
                            $titleHtml = '<a class="calender-event-title" href="#event'.$increment.'">'.$title.' (Day '.$dayCnt.')</a>';
                            }
                            $eventData[] = array(
                                                'eventName' =>  $titleHtml,
                                                'calendar'  => 'Work',
                                                'color'     => 'orange',
                                                'date'      =>  $eventDate
                                            );
                    }
                    $lastmonth = $thismonth;

                    edit_post_link('Edit this', ' <p class="edit-link">', '</p>');
                    if($i==$cols):
                        $i = 1;
                        echo '</div> <!-- /.row -->';
                    else:
                        $i++;
                    endif;
                    ?>  
                </div><!--end list-post-->
                <?php 
                
                $increment++;
                // endforeach; 
                endwhile;
                wp_reset_postdata();
        } else { ?>
            <p>No posts found.</p>
        <?php }?> 
    </div>
    </div>
            <!-- Sidebar Widgets Column -->
            
    <div class="right-div col-lg-4 col-md-5 col-sm-12" id="sideMenuDesktop">
        <div id="sideMenu">
            <div id="calendar"></div>
        </div>
    </div>
</div>


<script type="text/JavaScript">
    var data = <?php echo json_encode($eventData);?>;
    function showModal(modalData)
    {
       //you can do anything with data, or pass more data to this function. i set this data to modal header for example
       $("#myModal .modal-title").html(modalData.title);
       $("#myModal .event-duration").html(modalData.duration);
       $("#myModal .event-details").html(htmlspecialchars_decode(modalData.description));
       $("#myModal .event-location").html(modalData.location);
       $("#myModal .event-website").attr('href',modalData.website);
       $("#myModal .event-website").html(modalData.website);
       $("#myModal .event-contact").html(modalData.contact);
       $("#myModal .event-email").attr('href','mailto:'+modalData.email);
       $("#myModal .event-email").html(modalData.email);
       $("#myModal .event-telephone").html(modalData.telephone);
       $("#myModal .event-telephone").attr('href','tel:'+modalData.telephone);
       $("#myModal .event-permalink").attr('href',modalData.permalink);
       $("#myModal").modal();
    }
</script>
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/event/navbar.js" type="application/javascript"></script>
<?php require_once('footer.php');
