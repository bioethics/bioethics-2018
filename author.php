<?php require_once('header.php'); ?>
<div class="row blog-list-container">
    <div class="left-div col-lg-8 col-md-7 col-sm-12">
        <?php if ( have_posts() ) : ?>
            <h3 class="section-title">
                <span>
                    Author Archive: <?php echo '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a>';?>
                </span>                
                <div class="bg-title"></div>
            </h3>


            <?php
                // If a user has filled out their description, show a bio on their entries.
                if ( get_the_author_meta( 'description' ) || get_the_author_meta( 'user_url' ) ) : 
            ?>
                    <div id="author-info">
                      <div id="author-description">
                        <h2>About <?php echo get_the_author(); ?></h2>
                        <p id="author-bio"><?php the_author_meta( 'description' ); ?></p>
                        <?php if ( get_the_author_meta( 'user_url' ) ) : ?>
                          <p id="author-link"><a href="<?php the_author_meta( 'user_url' ); ?>" target="_blank">Website</a></p>
                        <?php endif; ?>
                      </div><!-- #author-description  -->
                    </div><!-- #author-info -->
            <?php endif; ?>

            <div id="main-content" class="row blog-list-row">
                <?php get_template_part('loop'); ?>
            </div>
        <?php else : ?>
            <h1>No Posts</h1> 
        <?php endif; ?>

    </div>

    <div class="col-md-4">
      <?php
      // if blog, show blog sidebar  
      if (get_post_type() == 'post' && is_active_sidebar('blog-sidebar')):
        ?>
        <div id="sidebar-blog" class="sidebar section">
          <ul>
            <?php dynamic_sidebar('blog-sidebar'); ?>
          </ul>
        </div>
        <?php
      endif;
      ?>
    </div>
</div>
<?php require_once('footer.php'); ?>