<?php
	global $posts_per_page, $has_post_type, $wp_query, $myposts;


	// separate results by type
	$i = 0;
	$x = 0;
	while ( have_posts() ) : the_post(); 
		$type = get_post_type();
		$myposts[$type][$i] = $post;
		if($type=='post' && $x < 10):
			$myposts[$type][$i]->excerpt = the_advanced_excerpt('length=20&exclude_tags=img',true);
			$x++;
		endif;
		$i++;
	endwhile; /* rewind or continue if all posts have been fetched */ 
	
	function post_count($type){
		global $has_post_type, $myposts, $wp_query;
		if(!$has_post_type):
			$count = count($myposts[$type]);
		else:
			$count = $wp_query->found_posts; 
		endif;
		return $count;
	}
	$i = 0;
	$content_type = 'post';
	
	foreach($myposts as $key=>$value){
		$t = get_post_type_object($key);
		$t_count = post_count($key);
		$mytypes[$i]['plural'] = $t->labels->name; 
		$mytypes[$i]['key'] = $key;
		if ($key != 'issues' && !isset($myposts['post']))
			$content_type = $key;
		$mytypes[$i]['count'] = $t_count;
		$i++;
	}
	
	function sortByOrder($a, $b) {
		return $a['count'] - $b['count'];
	}

	if (isset($_GET['post_type'])) { 
		$search_type = $_GET['post_type']; 
	} else {
		$search_type = $content_type;
	}  
	

	if($myposts): ?>
    
    <div class="row tag-search-row search-row">
        <div class="col-xs-12">
            <ul class="nav nav-tabs">
            	<?php if(isset($myposts['post'])): ?>
                	<li <?php if ( $search_type == 'post') : ?> class="active"<?php endif; ?>><a data-toggle="tab" href="#tag-posts">Posts (<?= post_count('post'); ?>)</a></li>
                <?php endif; ?>
                <?php if(isset($myposts['articles'])): ?>
                	<li <?php if ($search_type == 'articles') : ?> class="active"<?php endif; ?>>
                		<a data-toggle="tab" href="#tag-articles">Articles (<?= post_count('articles'); ?>)</a>
                	</li>
                <?php endif; ?>
                <?php if(isset($myposts['links'])): ?>
                	<li <?php if ($search_type == 'links') : ?> class="active"<?php endif; ?>>
                		<a data-toggle="tab" href="#tag-links">Resources (<?= post_count('links'); ?>)</a>
                	</li>
                <?php endif; ?>
                <?php if(isset($myposts['jobs'])): ?>
                	<li <?php if ($search_type == 'jobs') : ?> class="active"<?php endif; ?>>
                		<a data-toggle="tab" href="#tag-jobs">Jobs (<?= post_count('jobs'); ?>)</a>
                	</li>
                <?php endif; ?>
                <?php if(isset($myposts['news'])): ?>
                	<li <?php if ($search_type == 'news') : ?> class="active"<?php endif; ?>>
                		<a data-toggle="tab" href="#tag-news">News (<?= post_count('news'); ?>)</a>
                	</li>
                <?php endif; ?>
                <?php if(isset($myposts['page'])): ?>
                	<li <?php if ($search_type == 'page') : ?> class="active"<?php endif; ?>>
                		<a data-toggle="tab" href="#tag-pages">Pages (<?= post_count('page'); ?>)</a>
                	</li>
                <?php endif; ?>
                <?php if(isset($myposts['events'])): ?>
                	<li <?php if ($search_type == 'events') : ?> class="active"<?php endif; ?>>
                		<a data-toggle="tab" href="#tag-events">Events (<?= post_count('events'); ?>)</a>
                	</li>
                <?php endif; ?>
            </ul>
        </div>

		<div class="col-xs-12 tab-content tag-content-tag-search">
			<?php if (isset($myposts['post'])): ?>
			<div id="tag-posts" class="row tab-pane fade<?php if ($search_type == 'post') : ?> in active<?php endif; ?>">
				<?php
				$i = 0;
				foreach($myposts['post'] as $p): ?>
				<div class="col-lg-12">
		            <div id="blog-list1" class="card tag-search-card">
		                <div class="card-body">
		                    <div class="tag-search-title">
		                        <span class="date"><?= date('F j, Y',strtotime($p->post_date)); ?></span>
		                        <a href="<?= get_permalink($p->ID); ?>" title="<?= $p->post_title; ?>">
									<?= $p->post_title; ?>
								</a>
		                    </div>
		                    <div class="author">Bioethics.net Blog</div>
		                    <p>
		                        <?= $p->excerpt ?>
		                    </p>


		                    <a href="<?= get_permalink($p->ID); ?>" title="<?= $p->post_title; ?>" class="btn btn-primary btn-blog-list-details">Full Article</a>
		                </div>

		                <div class="card-footer">
		                    <?php 
		                    	$categories_list = get_the_category_list( ', ', '', $p->ID);
								$tag_list = get_the_tag_list( '', ', ', '', $p->ID);
		                    	
								if ( '' != $tag_list ) {
									$utility_text = 'This entry was posted in %1$s and tagged %2$s.  Posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
								} elseif ( '' != $categories_list ) {
									$utility_text = 'This entry was posted in %1$s. Posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
								} else {
									$utility_text = 'This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
								}
								printf(
									$utility_text,
									$categories_list,
									$tag_list,
									esc_url( get_permalink() ),
									the_title_attribute( 'echo=0' ),
									get_the_author(),
									esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
								); 
		                    ?>

		                </div>
						<?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$p->ID); ?>
					</div>
				</div>	
			<?php	$i++;
				if($i == 10) break;
				endforeach;
				if(count($myposts['post'])>10 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="'.$_SERVER['REQUEST_URI'].'/page/2/?post_type=post">View More Blog Entries</a></p>';
			?>
			</div>
			<?php
			endif;
		
			if(isset($myposts['articles'])): ?>
			<div id="tag-articles" class="row tab-pane fade<?php if ($search_type == 'articles') : ?> in active<?php endif; ?>">
				<?php
				$i = 0;
				foreach($myposts['articles'] as $a):
					$article_fields = get_fields($a->ID); 
					$journal_id = $article_fields['ajob_issue']->ID;
					$journal_fields = get_fields($journal_id);
					?>
					<div class="col-xs-12">
						<div  id="blog-list1" class="card tag-search-card">
							<div class="card-body">
								<div class="tag-search-title">
		                            <div class="date"><?php
										$tax = wp_get_post_terms($journal_id,'editions','name');
										echo $tax[0]->name;
										?>: Volume <?=$journal_fields['volume']; ?> Issue <?= $journal_fields['number']?> - <?php
										echo date('M Y',strtotime($journal_fields['publish_date'])); ?></div>
		                            <br/>
		                            <a href="https://www.tandfonline.com/openurl?genre=article&issn=<?=$journal_fields['isbn']?>&volume=<?=$journal_fields['volume']?>&issue=<?=$journal_fields['number'];?>&spage=<?=$firstpage?>" target="_blank">
		                            	<?= $a->post_title ?>	
		                            </a>
		                        </div>
		                        <div class="author"><?= $article_fields['primary_author'] ?></div>
		                        <a href="https://www.tandfonline.com/openurl?genre=article&issn=<?=$journal_fields['isbn']?>&volume=<?=$journal_fields['volume']?>&issue=<?=$journal_fields['number'];?>&spage=<?=$firstpage?>"  target="_blank" class="btn btn-primary btn-blog-list-details">Full Article</a>
		                        <?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$a->ID);?>
							</div>
						</div>	
					</div>
			<?php	$i++;
					if($i == 10) break;
				endforeach;
				echo '<br clear="both">';
				if(count($myposts['articles'])>$posts_per_page-1 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="?post_type=articles">View More Articles</a></p>';
			?>
			</div>
			<?php
			endif;
			
		
		
			if($myposts['links']): 
				$url = get_post_meta($post->ID, 'url', true);
			    $soruce = get_post_meta($post->ID, 'source', true);
			    $excerpt = get_post_meta($post->ID, 'excerpt', true);
			?>
			<div id="tag-links" class="row tab-pane fade<?php if ($search_type== 'links') : ?> in active<?php endif; ?>">
				
				<?php
				$i = 0;
				foreach($myposts['links'] as $link): ?>
	            <div class="col-lg-12">
	                <div id="" class="card search-card">
	                    <div class="card-body">
	                    	<h5 class="date"><?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$job->ID); ?></h5>
	                        <div class="search-title">
	                            <a href="<?= $url ?>" title="<?= $link->post_title ?>">
	                            	<?= $link->post_title ?>
	                            </a>
	                       		<?php if(isset($source)) echo '<span>'.$source.'</span>' ?>
	                        </div>
	                    </div>
	                </div>
	            </div>

			<?php	$i++;
					if($i == 10) break;
				endforeach;
				if(count($myposts['links'])>10 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="?post_type=links">View More Resources</a></p>';
			?>
			</div>
			<?php
			endif;
		
		
		
		
		if(isset($myposts['jobs'])): ?>
		<div id="tag-jobs" class="row tab-pane fade<?php if ($search_type== 'jobs') : ?> in active<?php endif; ?>">
			<?php
			$i = 0;
			foreach($myposts['jobs'] as $job):
				$fields = get_fields($job->ID); ?>

		    <div class="col-lg-12 jobs">
	            <div id="" class="card search-card">
	                <div class="card-body">
	                	<h5 class="date"><?= $fields['company_name'] ?><?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$job->ID); ?></h5>
	                    <div class="search-title">
	                        <a href="<?= get_permalink($job->ID) ?>" title="<?= $job->post_title ?>">
	                        	<?= $job->post_title ?>
	                        </a>
	                   		<?php if(isset($source)) echo '<span>'.$source.'</span>' ?>
	                    </div>
	                </div>
	            </div>
	        </div>
		<?php	$i++;
				if($i == 10) break;
			endforeach;
			if(count($myposts['jobs'])>10 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="?post_type=jobs">View More Jobs</a></p>';
		?>
		</div>
		<?php
		endif;
		
		
		
		
		
		if(isset($myposts['news'])): ?>
		<div id="tag-news" class="row tab-pane fade<?php if ($search_type== 'news') : ?> in active<?php endif; ?>">
			<?php
			$i = 0;
			foreach($myposts['news'] as $n):
				$fields = get_fields($n->ID); ?>

	        <div class="col-lg-12">
	            <div id="blog-list1" class="card tag-search-card">
	                <div class="card-body">
	                    <div class="tag-search-title">
	                        <span class="date"><?= date('F j, Y g:i a',strtotime($n->post_date)); ?></span>
	                        <a href="<?= $fields['url'] ?>" target="_blank" title="<?= $n->post_title ?>">
								<?= $n->post_title ?>
							</a>
	                    </div>
	                    <?php if($fields['source']): ?>
	                    	<div class="author"><?= $fields['source']; ?></div>
	                    <?php endif; ?>
	                    <?php if($fields['excerpt']):?>
							<p><?=$fields['excerpt'];?></p>
						<?php endif; ?>
	                    <a href="<?= $fields['url'] ?>" target="_blank" class="btn btn-primary btn-blog-list-details">Full Article</a>
	                    <?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$n->ID);?>
	                </div>
	                
	            </div>
	        </div>	
		<?php	$i++;
				if($i == 10) break;
			endforeach;
			if(count($myposts['news'])>10 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="?post_type=news">View More News Items</a></p>';
		?>
		</div>
		<?php
		endif;
		
		
		
		if(isset($myposts['page'])): ?>
		<div id="tag-pages" class="row tab-pane fade<?php if ($search_type== 'page') : ?> in active<?php endif; ?>">
			<?php
			$i = 0;
			foreach($myposts['page'] as $p): ?>
	        <div class="col-lg-12">
	            <div id="" class="card search-card">
	                <div class="card-body">
	                    <div class="search-title">
	                        <a href="<?= get_permalink($p->ID); ?>" title="<?= $p->post_title ?>">
	                        	<?= $p->post_title ?>
	                        </a>
	                    </div>
	                    <?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$p->ID);?>
	                </div>
	            </div>
	        </div>
		<?php	$i++;
				if($i == 10) break;
			endforeach;
			if(count($myposts['page'])>10 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="?post_type=page">View More Pages</a></p>';
		?>
		</div>
		<?php
		endif;
		
		
		
		if(isset($myposts['events'])): ?>
		<div id="tag-events" class="row tab-pane fade<?php if ($search_type== 'events') : ?> in active<?php endif; ?>">
			
			<?php
			$i = 0;
			foreach($myposts['events'] as $e):
				$fields = get_fields($e->ID); ?>

	            <div class="col-lg-12">
	                <div id="" class="card tag-search-card search-card">
	                    <div class="card-body">
	                        <div class="search-title tag-search-title">
	                            <span class="date"><?= date('M d, Y',strtotime($e->post_date)); ?></span>
	                            <a href="<?= get_permalink($e->ID) ?>" title="<?= $e->post_title ?>"><?= $e->post_title ?></a>
	                            <?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$e->ID); ?>
	                        </div>
	                        <div class="location">
	                        	<?php
									$location = array();
									$city =	$fields['city'];
									if($city && $city !='') $location[] = $city;
									$state = $fields['state'];
									if($state && $state !='') $location[] = $state;
									$province = $fields['province'];
									if($province && $province!='') $location[] = $province;
									$country = $fields['country'];
									if($country && $country !='') $location[] = $country;

									echo implode(', ',$location);
								?>
	                        </div>
	                    </div>
	                </div>
	            </div>	
		<?php	$i++;
				if($i == 10) break;
			endforeach;
			if(count($myposts['events'])>10 && $has_post_type==false) echo '<p class="more col-md-12"><a class="more" href="?post_type=events">View More Events</a></p>';
		?>
		</div>
		<?php
		endif;
		?>
	</div><!-- End of tag-content-tag-search -->
</div><!-- End of tag-search-row -->
<?php else: ?>
	<div>
		No posts found
	</div>
<?php endif; ?>

