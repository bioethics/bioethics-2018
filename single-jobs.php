<?php require_once('header.php'); ?>
<div class="row job-details-container">
	<div id="job-content" class="left-div col-lg-9 col-md-9 col-sm-12">
	    <h3 class="section-title">
	        <span>Bioethics Jobs</span>
	        <span>&nbsp;&nbsp;<a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs" id="subnav_current">Current Bioethics Jobs</a>  | <a href="<?php bloginfo('wpurl')?>/submit_job" id="subnav_submit">Submit a Job</a></span>                    
	        <div class="bg-title"></div>
	    </h3>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); 
			$company = get_field('institution_name');
			$deadline = get_field('application_deadline');
			$deadline_text = get_field('application_deadline_text');
			$description = get_field('long_description');
			$city =	get_field('city');
			$state = get_field('state');
			$province = get_field('province');
			$country = get_field('country');
			
			$name = get_field('name');
			$tele = get_field('telephone');
			$email = get_field('email'); 
		?>

	        <div class="row job-details-row" id="post-<?php the_ID(); ?>">
	            <div class="col-xs-12">

	                <b><small>Posted: <?php echo get_the_date('l, F jS, Y');?></small></b>
	                <table class="table">
	                    <tbody>
	                        <tr>
	                            <td>JOB TITLE:</td>
	                            <td>
	                            	<strong>
		                            	<a href="<?php the_permalink(); ?>" rel="bookmark">
											<?php the_title(); ?>
										</a>
									</strong>
								</td>
	                        </tr>
	                        <?php if($deadline): ?>
	                        <tr>
	                            <td>DEADLINE:</td>
	                            <td><?= ($deadline_text!='') ? $deadline_text : date('l, F jS, Y',  strtotime($deadline))?></td>
	                        </tr>
	                        <?php endif; ?>
	                        <tr>
	                            <td>LOCATION:</td>
	                            <td>
	                                <?=$company?>
	                                <br>
	                                <?php 
										$location = array();
										if($city && $city !='') $location[] = $city;
										if($state && $state !='') $location[] = $state;
										if($province && $province!='') $location[] = $province;
										if($country && $country !='') $location[] = $country;
										echo implode(', ',$location);
									?>
	                            </td>
	                        </tr>

	                        <tr>
	                            <td>
	                                CONTACT:
	                            </td>
	                            <td>
	                                <?=$name;?>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                EMAIL:
	                            </td>
	                            <td>
	                                <a href="mailto:<?=$email?>"><?=$email?></a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                TELEPHONE:
	                            </td>
	                            <td>
	                                <a href="tel:<?=$tele;?>"><?=$tele;?></a>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <div><b>DETAILS:</b></div>
	                <div class="job-details">
	                	<?= $description ?>	
	                </div>

					<div class="meta">
							<p>
						<?php
						$categories_list = get_the_category_list( ', ');
						$tag_list = get_the_tag_list( '', ', ');
						if ( '' != $tag_list ) {
							$utility_text = 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
						} elseif ( '' != $categories_list ) {
							$utility_text = 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
						} else {
							$utility_text = 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
						}

						printf(
							$utility_text,
							$categories_list,
							$tag_list,
							esc_url( get_permalink() ),
							the_title_attribute( 'echo=0' )
						);
						edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>
							</p>
					</div>
					<div class="social">
						<?php //if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
					</div>

	            </div>

			</div><!--end post-->
		<?php endwhile; ?>
		
		<div id="pagination">
		<?php if(is_single()): ?>
			<span class="nav-new">
				<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
			</span>
			<span class="nav-old">
				<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
			</span>
		<?php endif; ?>
		</div><!-- /#pagination-->
	<?php else : ?>
		<h1>No Posts!</h1>	
	<?php endif; ?>

	</div>
</div>
<?php
require_once('footer.php');