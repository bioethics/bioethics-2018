
			<div class="banner-ad">
				<!-- Bioethics banner footer [async] -->
				<script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
				<script type="text/javascript">
				var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
				var abkw = window.abkw || '';
				var plc182357 = window.plc182357 || 0;
				document.write('<'+'div id="placement_182357_'+plc182357+'"></'+'div>');
				AdButler.ads.push({handler: function(opt){ AdButler.register(164087, 182357, [728,90], 'placement_182357_'+opt.place, opt); }, opt: { place: plc182357++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
				</script>
			</div>
		</div><!--end .container -->

		<footer class="py-5 footer">
			<div class="footer-top">
				<div class="container text-center">

					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12 text-center">
							<h4>Sign up for <br/><a href="/newsletter/">Bioethics Today</a></h4>
							<a id="footer-subscribe-button" href="/newsletter/"><span class="btn btn-white">Sign Up</span></a>
							<!-- <a id="footer-subscribe-archive" href="/newsletter/">Archives</a> -->
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12 bordered-col">
							<h4>
								<a href="<?php bloginfo('wpurl')?>/events" title="Upcoming Bioethics Events">Upcoming events.</a>
							</h4>
							<div class="row">
							<?php 
								$footer_events = get_transient( 'footer_events');
								if(false === $footer_events):
									$today = date('Y/m/d');
									$vars['order'] = 'ASC';
									$vars['posts_per_page'] = 2;
									$vars['post_type']		= 'events';
									$vars['meta_type'] = 'DATE';
									$vars['meta_key'] = 'start_date';
									$vars['orderby'] = 'meta_value_num';
									$vars['meta_query'] = array(
										array(
											'key' => 'end_date',
											'compare' => '>=',
											'value'	=> $today,
											'type'	=> 'DATE'
										)
									);
									$footer_events = new WP_Query($vars);
									set_transient('footer_events',$footer_events, HOUR_IN_SECONDS );
								endif;
								$i = 0;
								if ( $footer_events->have_posts() ) : 
									while ( $footer_events->have_posts() ) : $footer_events->the_post(); 
										$start = get_field('start_date');
										$end = get_field('end_date');
							?>

								<div class="col-xs-6">
									<div>
										<?php
										echo date('F jS',strtotime($start));
										if($end && $end != $start) echo " - ".date('F jS',strtotime($end));
										?>
									</div>
									<div><a href="<?php the_permalink()?>"><?php the_title() ?></a></div>
								</div>
							<?php		
									$i++;
									endwhile;
								endif;
							?>
							</div>
						</div>	

						<div id="col-md-3 col-sm-12 col-xs-12">
							<h4 class="text-center">
								<a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs">BIOETHICS JOBS.</a>
							</h4>
							<br />
							<a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs">
								<span class="positions">
									<?php echo get_current_jobs(true); ?>
								</span>
							
								<span>
									Positions currently listed
								</span>
							</a>
						</div>
					</div>
				</div><!--end #footer .container -->
			</div><!--end #footer -->
			<div class="container text-center footer-bottom">
	            <span class="m-0 text-center text-white">Copyright © Bioethics.net <?php echo date("Y");?></span>

			    <span class="social-icons">
			    	<a href="/about/rss-feeds"><i class="fa fa-rss"></i></a>
			        <a href="https://twitter.com/bioethics_net" target="_blank" title="Bioethics.net on Twitter"><i class="fa fa-twitter"></i></a>
			        <a href="https://www.facebook.com/groups/bioethics.net/" target="_blank" title="Bioethics.net on Facebook"><i class="fa fa-facebook"></i></a>
			    </span>
	        </div>
		</footer>

	<!-- </div> --><!--end #global-wrap -->
		<?php wp_footer();
		if (!WP_DEBUG):
			?>
			<script type="text/javascript">
				var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
				document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

			</script>
			<script type="text/javascript">
				try {
					var pageTracker = _gat._getTracker("UA-8124041-1");
					pageTracker._setDomainName("bioethics.net");
					pageTracker._trackPageview();
				} catch(err) {}</script>
		<?php else: ?>
			<script type="text/javascript">
				var disqus_developer = 1; // developer mode is on
			</script>
		<?php endif; ?>
	</body>
</html>
