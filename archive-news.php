<?php require_once('header.php'); ?>
<div id="list-content">
	<?php 
	if ( have_posts() ) : ?>
		<h3 class="section-title">
			Bioethics news.
            <div class="bg-title"></div>
        </h3>
        <div id="newsListing">
			<?php get_template_part( 'loop','short' ); ?>
		</div>
	<?php else : ?>
		<p>No posts found.</p>
	<?php endif; 
?>
</div><!--end content-->
<?php get_footer(); ?>
