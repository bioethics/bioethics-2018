<?php 
/**
 * Template Name: About
 *
 */

require_once('header.php'); ?>
<div class="row">
	<div class="col-md-4 resources-menu">
		<?php
		wp_nav_menu(array(
			'theme_location' => 'about_nav',
			'menu_id' => 'about-nav',
		));
		?>

	</div>
	<div class="col-md-8 resources-main">
		<h3 class="section-title">
            <i class="fa fa-chevron-right hidden-lg hidden-md"></i><?php the_title(); ?>
            <div class="bg-title"></div>
        </h3>
        <div class="entry">
        	<?php if (have_posts()) :
		        	 while (have_posts()) : the_post();
		        	 	the_content(); ?>
						<?php edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>	
						<div class="social">
							<?php if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
						</div>
						<?php endwhile;
			endif;?>
		</div>
	</div>
</div>
<?php require_once('footer.php');